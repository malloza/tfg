CREATE DATABASE if not exists soundMusic;
--
USE soundMusic;
--
Create table if not exists usuarios(
idUsuario int auto_increment primary key,
nombre varchar(15) not null,
apellido varchar(15) not null,
fechaNacimiento date not null,
numeroTelefono varchar(10) not null,
tipoDeUsuario varchar(20) not null,
nick varchar (15) not null,
pass varchar (15) not null);
--
Create table if not exists discograficas(
idDiscografica int auto_increment primary key,
nombre varchar(50) not null,
direccion varchar(150) not null,
codigoPostal varchar(10) not null,
cif varchar(20) not null,
numeroTelefono varchar(10),
correoElectronico varchar(30),
nombreDirector varchar(20));
--
Create table if not exists cantantes(
idCantante int auto_increment primary key,
nombre varchar(10) not null,
fechaNacimiento date not null,
nacionalidad varchar(15),
premios varchar(100),
genreo varchar(20),
sexo varchar(20),
formacion varchar(100),
discografica_id int,
FOREIGN KEY (discografica_id) references discograficas(idDiscografica));
--
Create table if not exists discos(
idDisco int auto_increment primary key,
nombre varchar(20) not null,
genero varchar(20) not null,
fechaEstreno date not null,
numeroCanciones int not null,
duracion varchar(10) not null,
featurings varchar(50),
precio double not null,
cantante_id int,
FOREIGN KEY (cantante_id) references cantantes (idCantante));
--
Create table if not exists canciones(
idCancion int auto_increment primary key,
titulo varchar(20) not null,
genero varchar(30) not null,
duracion varchar(10) not null,
featuring varchar(100),
reproducciones int,
explicita boolean not null,
precio double not null,
discos_id int,
FOREIGN KEY (discos_id) references discos (idDisco));

--
Insert into usuarios
values (1,'Manuel','Alloza','1996-01-01','651336100','administrador','manu','1234');

Insert into discograficas
values (1,'Ruthless','Grove Street','50020','454128','976445458','SFDK@gmail.com','DR DREE');

Insert into cantantes
values (1,'Eminem','1889-01-01','USA','Grammy','Rap','masculino','La calle',1);

Insert into discos
values (1,'Slim Shady','Rap','2001-02-02',12,'5 horas','ningua',4,1);

Insert into canciones
values (1,'Lost yourself','Rap','5:03min','ninguna',125,true,2,1);

select * from discograficas;
select * from canciones;
select * from usuarios;
select * from cantantes;