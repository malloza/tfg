package mvc;

import com.malloza.soundMusic.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;


public class Modelo {


    SessionFactory sessionFactory;

    /**
     * Metodo para desconectar con el servidor
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo para conectar con el servidor
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure();

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Usuario.class);
        configuracion.addAnnotatedClass(Cancion.class);
        configuracion.addAnnotatedClass(Cantante.class);
        configuracion.addAnnotatedClass(Disco.class);
        configuracion.addAnnotatedClass(Discografica.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Metodo para dar de alta usuario
     * @param usuario
     */
    public void altaUsuario(Usuario usuario){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(usuario);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para dar de alta discografica en la base de datos
     * @param discografica
     */
    public void altaDiscografica(Discografica discografica){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(discografica);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para dar de alta cantante en la base de datos
     * @param cantante
     */
    public void altaCantante(Cantante cantante ){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(cantante);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para dar de alta disco en la base de datos
     * @param disco
     */
    public void altaDisco(Disco disco){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(disco);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para dar de alta cancion en la base de datos
     * @param cancion
     */
    public void altaCancion(Cancion cancion ){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(cancion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un usuario en la base de datos
     * @param usuarioSeleccionado
     */
    public void modificarUsuario(Usuario usuarioSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(usuarioSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un usuario en la base de datos
     * @param discograficaSeleccionada
     */
    public void modificarDiscografica(Discografica discograficaSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(discograficaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar cantente en la base de datos
     * @param cantanteSeleccionado
     */
    public void modificarCantante(Cantante cantanteSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(cantanteSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un disco en la base de datos
     * @param discoSeleccionado
     */
    public void modificarDisco(Disco discoSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(discoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para modificar un cancion en la base de datos
     * @param cancionSeleccionada
     */
    public void modificarCancion(Cancion cancionSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(cancionSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para eliminar usuario en la base de datos
     * @param usuarioSeleccionado
     */
    public void eliminarUsuario(Usuario usuarioSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(usuarioSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para eliminar discografica en la base de datos
     * @param discograficaSeleccionada
     */
    public void eliminarDiscografica(Discografica discograficaSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(discograficaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para eliminar cantante en la base de datos
     * @param cantanteSeleccionada
     */
    public void eliminarCantante(Cantante cantanteSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cantanteSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para eliminar disco en la base de datos
     * @param discoSeleccionado
     */
    public void eliminarDisco(Disco discoSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(discoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo para eliminar cancion en la base de datos
     * @param cancionSeleccionada
     */
    public void eliminarCancion(Cancion cancionSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cancionSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que devuelve la lista de usuarios
     * @return lista
     */
    public ArrayList<Usuario> getUsuarios() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario");
        ArrayList<Usuario> lista = (ArrayList<Usuario>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que devuelve la lista de cantantes
     * @return lista
     */
    public ArrayList<Discografica> getDiscograficas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Discografica");
        ArrayList<Discografica> lista = (ArrayList<Discografica>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que devuelve la lista de cantantes
     * @return lista
     */
    public ArrayList<Cantante> getCantantes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cantante ");
        ArrayList<Cantante> lista = (ArrayList<Cantante>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que devuelve la lista de cantantes
     * @return lista
     */
    public ArrayList<Disco> getDiscos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Disco ");
        ArrayList<Disco> lista = (ArrayList<Disco>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que devuelve la lista de cantantes
     * @return lista
     */
    public ArrayList<Cancion> getCanciones() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cancion ");
        ArrayList<Cancion> lista = (ArrayList<Cancion>)query.getResultList();
        sesion.close();
        return lista;
    }

}
