package mvc;

import com.malloza.soundMusic.base.*;
import mvc.vistas.Login;
import mvc.vistas.Registro;
import mvc.vistas.VistaMain;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase controlador que une la vista grafica con las operaciones del modelo y refresca la informacion de las diferentes vistas
 */
public class Controlador implements ActionListener,ListSelectionListener, KeyListener {
    private Login login;
    private Registro registro;
    private VistaMain vista;
    private Modelo modelo;
    private int tipoVentanaEmergente;
    Connection conexion = null;

    /**
     * Cosntructor de la clase controlador
     * @param login
     * @param registro
     * @param vista
     * @param modelo
     */
    public Controlador(Login login, Registro registro, VistaMain vista, Modelo modelo) {
        this.login = login;
        this.registro = registro;
        this.vista = vista;
        this.modelo = modelo;
        tipoVentanaEmergente=1;

        login.setVisible(true);
        registro.setVisible(false);
        vista.setVisible(false);

        modelo.conectar();

        addActonListener();
        addListSelectionListener();
        addKeySelectionListener();

        refrescarListaUsuarios(modelo.getUsuarios());
    }

    /**
     * Constructor vacio de la clase controlador
     */
    public Controlador(){

    }

    /**
     * Metodo para anadir los campos de texto de los buscadores en el KeySelecctionListener
     */
    private void addKeySelectionListener(){
        vista.txtValorCancionBuscar.addKeyListener(this);
        vista.txtValorDiscoBuscar.addKeyListener(this);
        vista.txtValorCantanteBuscar.addKeyListener(this);
        vista.txtValorDiscograficaBuscar.addKeyListener(this);
    }

    /**
     * Metodoe para anadir las listas a los ListSelectionListener
     */
    private void addListSelectionListener() {
        vista.listDiscograficas.addListSelectionListener(this);
        vista.listCantantes.addListSelectionListener(this);
        vista.listDiscos.addListSelectionListener(this);
        vista.listCanciones.addListSelectionListener(this);
        vista.listCantantesContratados.addListSelectionListener(this);
        vista.listDiscosCantante.addListSelectionListener(this);
        vista.listCancionesDisco.addListSelectionListener(this);
        vista.listUsuariosApp.addListSelectionListener(this);
    }

    /**
     * Metodo que anadir los actionListener a los botones de las vistas
     */
    public void addActonListener(){

        //login
        login.btnRegistro.addActionListener(this);
        login.btnAcceder.addActionListener(this);
        login.btnSalir.addActionListener(this);

        //register
        registro.btnAceptar.addActionListener(this);
        registro.btnCancelar.addActionListener(this);

        //alta modificar y eliminar
        //discografica
        vista.btnAnadirDiscografica.addActionListener(this);
        vista.btnModificarDiscografica.addActionListener(this);
        vista.btnEliminarDiscografica.addActionListener(this);
        //cantante
        vista.btnAnadirCantante.addActionListener(this);
        vista.btnModificarCantante.addActionListener(this);
        vista.btnEliminarCantante.addActionListener(this);
        //disco
        vista.btnAnadirDisco.addActionListener(this);
        vista.btnModificarDisco.addActionListener(this);
        vista.btnEliminarDisco.addActionListener(this);
        //cancion
        vista.btnAnadirCancion.addActionListener(this);
        vista.btnModificarCancion.addActionListener(this);
        vista.btnEliminarCancion.addActionListener(this);

        //opciones
        vista.btnInformesCantantes.addActionListener(this);
        vista.btnInformeReproduciones.addActionListener(this);
        vista.rbTemaPredeterminado.addActionListener(this);
        vista.rbPop.addActionListener(this);
        vista.rbRap.addActionListener(this);
        vista.btnCambiatUsuario.addActionListener(this);

        //ventana de usuarios
        vista.btnModificarUsuario.addActionListener(this);
        vista.btnEliminarUsuario.addActionListener(this);
    }

    /**
     * Metodo que se le pasa de parametro el ActionCommand del boton seleccionado en la vista y realiza yna serie de operaciones
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case"acceder":
                Usuario usarioAcceder = buscarUsuarioPorNombre(login.txtUsuario.getText());
                if(comprobarExistenciaUsuario(login.txtUsuario.getText(),login.pwLogin.getText())){
                    controlarPermisosAplicacion(usarioAcceder);
                    login.setVisible(false);
                    vista.setVisible(true);
                }
                limpiarCamposLogin();
                refrescarListaUsuarios(modelo.getUsuarios());
                break;
            case"registrar":
                login.setVisible(false);
                registro.setVisible(true);
                break;
            case"aceptar":
                altaUsuario();
                registro.setVisible(false);
                login.setVisible(true);
                refrescarListaUsuarios(modelo.getUsuarios());
                limpiarCamposRegistro();
                break;
            case"cancelar":
                registro.setVisible(false);
                login.setVisible(true);
                break;
            case"salirLogin":
                modelo.desconectar();
                System.exit(0);
                break;
            case"altaDiscografica":
                Discografica discografica=new Discografica();
                discografica.setNombre(vista.txtNombreDiscografica.getText());
                discografica.setDireccion(vista.txtDireccion.getText());
                discografica.setCodigoPostal(vista.txtCodigoPostal.getText());
                discografica.setNombreDirector(vista.txtNombreDirectorDiscografica.getText());
                discografica.setCif(vista.txtCif.getText());
                discografica.setCorreoElectronico(vista.txtCorreoElectronico.getText());
                discografica.setNumeroTelefono(vista.txtTelefonoDiscografica.getText());
                modelo.altaDiscografica(discografica);
                limpiarCamposDiscografica();
                refrescarListaDiscograficas(modelo.getDiscograficas());
                refrescarComboBoxDiscograficas(modelo.getDiscograficas());
                break;
            case"altaCantante":
                Cantante cantante = new Cantante();
                cantante.setNombre(vista.txtNombreCantante.getText());
                cantante.setFechaNacimiento(vista.dpFechaNacimientoCantante.getDate());
                cantante.setNacionalidad(vista.txtNacionalidad.getText());
                cantante.setFormacion(vista.txtFormacion.getText());
                cantante.setPremios(vista.txtPremios.getText());
                cantante.setGenreo(vista.txtGeneroCantante.getText());
                if(vista.rbMasculino.isSelected()==true){
                    cantante.setSexo("masculino");
                }else{
                    cantante.setSexo("femeninio");
                }
                Discografica discograficaElejida = (Discografica) vista.comboBoxDiscograficas.getSelectedItem();
                cantante.setDiscografica(discograficaElejida);
                modelo.altaCantante(cantante);
                refrescarListaCantantes(modelo.getCantantes());
                refrescarComboBoxCantantes(modelo.getCantantes());
                limpiarCamposCantantes();
                break;
            case"altaDisco":
                Disco disco = new Disco();
                disco.setNombre(vista.txtNombreDisco.getText());
                disco.setGenero(vista.txtGeneroDisco.getText());
                disco.setDuracion(vista.txtDuracion.getText());
                disco.setFechaEstreno(vista.dpFechaLanzamiento.getDate());
                disco.setDuracion(vista.txtDuracionDisco.getText());
                disco.setFeaturings(vista.txtFeaturings.getText());
                disco.setNumeroCanciones(Integer.parseInt(vista.txtNumeroCanciones.getText()));
                disco.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                Cantante cantanteElejido = (Cantante) vista.comboBoxCantantes.getSelectedItem();
                disco.setCantante(cantanteElejido);
                modelo.altaDisco(disco);
                limpiarCamposDisco();
                refrescarListaDiscos(modelo.getDiscos());
                refrescarComboBoxDiscos(modelo.getDiscos());
                break;
            case"altaCancion":
                Cancion cancion = new Cancion();
                cancion.setTitulo(vista.txtTituloCancion.getText());
                cancion.setGenero(vista.txtGenero.getText());
                cancion.setDuracion(vista.txtDuracion.getText());
                 if(vista.rbSi.isSelected()){
                     cancion.setExplicita(true);
                 }else{
                     cancion.setExplicita(false);
                 }
                cancion.setFeaturing(vista.txtFeaturingsCancion.getText());
                cancion.setPrecio(Double.parseDouble(vista.txtPrecioCancion.getText()));
                cancion.setReproducciones(Integer.parseInt(vista.txtReproducciones.getText()));
                Disco discoElejido = (Disco) vista.comboBoxDiscos.getSelectedItem();
                cancion.setDisco(discoElejido);
                modelo.altaCancion(cancion);
                limpiarCamposCancion();
                refrescarListaCanciones(modelo.getCanciones());
                break;
            case"modificarDiscografica":
                Discografica discograficaSeleccionada= (Discografica) vista.listDiscograficas.getSelectedValue();
                discograficaSeleccionada.setNombre(vista.txtNombreDiscografica.getText());
                discograficaSeleccionada.setDireccion(vista.txtDireccion.getText());
                discograficaSeleccionada.setCodigoPostal(vista.txtCodigoPostal.getText());
                discograficaSeleccionada.setNombreDirector(vista.txtNombreDirectorDiscografica.getText());
                discograficaSeleccionada.setCif(vista.txtCif.getText());
                discograficaSeleccionada.setCorreoElectronico(vista.txtCorreoElectronico.getText());
                discograficaSeleccionada.setNumeroTelefono(vista.txtTelefonoDiscografica.getText());
                modelo.modificarDiscografica(discograficaSeleccionada);
                refrescarListaDiscograficas(modelo.getDiscograficas());
                refrescarComboBoxDiscograficas(modelo.getDiscograficas());
                limpiarCamposDiscografica();
                break;
            case"modificarCantante":
                Cantante cantanteSeleccionado= (Cantante) vista.listCantantes.getSelectedValue();
                cantanteSeleccionado.setNombre(vista.txtNombreCantante.getText());
                cantanteSeleccionado.setFechaNacimiento(vista.dpFechaNacimientoCantante.getDate());
                cantanteSeleccionado.setNacionalidad(vista.txtNacionalidad.getText());
                cantanteSeleccionado.setFormacion(vista.txtFormacion.getText());
                cantanteSeleccionado.setGenreo(vista.txtGeneroCantante.getText());
                if(vista.rbMasculino.isSelected()==true){
                    cantanteSeleccionado.setSexo("Masculino");
                }else{
                    cantanteSeleccionado.setSexo("Femeninio");
                }
                modelo.modificarCantante(cantanteSeleccionado);
                refrescarListaCantantes(modelo.getCantantes());
                refrescarComboBoxCantantes(modelo.getCantantes());
                limpiarCamposCantantes();
                break;
            case"modificarDisco":
                Disco discoSeleccionado= (Disco) vista.listDiscos.getSelectedValue();
                discoSeleccionado.setDuracion(vista.txtDuracion.getText());
                discoSeleccionado.setFechaEstreno(vista.dpFechaLanzamiento.getDate());
                discoSeleccionado.setDuracion(vista.txtDuracionDisco.getText());
                discoSeleccionado.setFeaturings(vista.txtFeaturings.getText());
                discoSeleccionado.setGenero(vista.txtGeneroDisco.getText());
                discoSeleccionado.setNumeroCanciones(Integer.parseInt(vista.txtNumeroCanciones.getText()));
                discoSeleccionado.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                modelo.modificarDisco(discoSeleccionado);
                refrescarListaDiscos(modelo.getDiscos());
                refrescarComboBoxDiscos(modelo.getDiscos());
                limpiarCamposDisco();
                break;
            case"modificarCancion":
                Cancion cancionSeleccionada= (Cancion) vista.listCanciones.getSelectedValue();
                cancionSeleccionada.setTitulo(vista.txtTituloCancion.getText());
                cancionSeleccionada.setGenero(vista.txtGenero.getText());
                cancionSeleccionada.setDuracion(vista.txtDuracion.getText());
                if(vista.rbSi.isSelected()){
                    cancionSeleccionada.setExplicita(true);
                }else{
                    cancionSeleccionada.setExplicita(false);
                }
                cancionSeleccionada.setFeaturing(vista.txtFeaturingsCancion.getText());
                cancionSeleccionada.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                cancionSeleccionada.setReproducciones(Integer.parseInt(vista.txtReproducciones.getText()));
                modelo.modificarCancion(cancionSeleccionada);
                refrescarListaCanciones(modelo.getCanciones());
                limpiarCamposCancion();
                break;
            case"eliminarDiscografica":
                Discografica discograficaEliminada = (Discografica) vista.listDiscograficas.getSelectedValue();
                vista.dlmDiscograficas.removeElement(discograficaEliminada);
                modelo.eliminarDiscografica(discograficaEliminada);
                refrescarListaDiscograficas(modelo.getDiscograficas());
                refrescarComboBoxDiscograficas(modelo.getDiscograficas());
                limpiarCamposDiscografica();
                break;
            case"eliminarCantante":
                Cantante cantanteEliminado = (Cantante) vista.listCantantes.getSelectedValue();
                vista.dlmCantantes.removeElement(cantanteEliminado);
                modelo.eliminarCantante(cantanteEliminado);
                refrescarListaCantantes(modelo.getCantantes());
                refrescarComboBoxCantantes(modelo.getCantantes());
                limpiarCamposCantantes();
                break;
            case"eliminarDisco":
                Disco discoEliminada = (Disco) vista.listDiscos.getSelectedValue();
                vista.dlmDiscos.removeElement(discoEliminada);
                modelo.eliminarDisco(discoEliminada);
                refrescarListaDiscos(modelo.getDiscos());
                refrescarComboBoxDiscos(modelo.getDiscos());
                limpiarCamposDisco();
                break ;
            case"eliminarCancion":
                Cancion cancionEliminada = (Cancion) vista.listCanciones.getSelectedValue();
                vista.dlmDiscograficas.removeElement(cancionEliminada);
                modelo.eliminarCancion(cancionEliminada);
                refrescarListaCanciones(modelo.getCanciones());
                limpiarCamposCancion();
                break;
            case "fotoCantante":
                JFileChooser fc = new JFileChooser();
                int seleccion = fc.showOpenDialog(fc);
                if(seleccion == JFileChooser.APPROVE_OPTION){
                    File fichero = fc.getSelectedFile();
                    vista.lblFotoCantante.setIcon(new ImageIcon(fichero.getAbsolutePath()));
                }
                break;
            case"fotoCaratula":
                JFileChooser fc1 = new JFileChooser();
                int seleccion1 = fc1.showOpenDialog(fc1);
                if(seleccion1 == JFileChooser.APPROVE_OPTION){
                    File fichero = fc1.getSelectedFile();
                    vista.lblFotoCaratula.setIcon(new ImageIcon(fichero.getAbsolutePath()));
                }
                break;
            case"informeReproducciones":

                break;
            case"informeCantantes":

                break;
            case"predeterminado":
                vista.cambiarColorPredeterminado();
            case"rap":
                vista.cambiarColorRap();
            case"pop":
                vista.cambiarColorPop();
            case"aplicarTema":
                temaAplicaion();
                break;
            case"cambiarUsuario":
                vista.setVisible(false);
                login.setVisible(true);
                break;
            case"modificarUsuario":
                Usuario user = (Usuario) vista.listUsuariosApp.getSelectedValue();
                if(vista.rbAdmin.isSelected()==true){
                    user.setTipoDeUsuario("administrador");
                }
                if(vista.rbNormal.isSelected()==true){
                    user.setTipoDeUsuario("normal");
                }
                if(vista.rbVisual.isSelected()==true){
                    user.setTipoDeUsuario("visual");
                }
                modelo.modificarUsuario(user);
                refrescarListaUsuarios(modelo.getUsuarios());
                break;
            case"eliminarUsuario":
                Usuario user1 = (Usuario) vista.listUsuariosApp.getSelectedValue();
                modelo.eliminarUsuario(user1);
                refrescarListaUsuarios(modelo.getUsuarios());
                break;

        }
        refrescarTodo();
        temaAplicaion();
    }

    /**
     * Metodo que cambia la vista main de la aplicacion segun la opcion seleccionda en Opciones
     */
    private void temaAplicaion() {
        if(vista.rbTemaPredeterminado.isSelected()==true){
            vista.cambiarColorPredeterminado();
        }
        if(vista.rbRap.isSelected()==true){
            vista.cambiarColorRap();
        }
        if(vista.rbPop.isSelected()==true){
            vista.cambiarColorPop();
        }
    }

    /**
     *Metodo que controla el tipo de usuario que entra en la aplicacion
     * @param usuario
     */
    private void controlarPermisosAplicacion(Usuario usuario) {
            if(usuario.getTipoDeUsuario().equals("administrador")){
                vista.cambiarVistaAdmin();
            }
            if(usuario.getTipoDeUsuario().equals("normal")){
                vista.cambiarVistaNomal();
            }
            if(usuario.getTipoDeUsuario().equals("visual")){
                vista.cambiarVistaVisual();
            }

    }

    /**
     *Metodo que da de alta un usuario
     */
    private void altaUsuario() {
        Usuario usuario = new Usuario();
        usuario.setNombre(registro.txtNombre.getText());
        usuario.setApellido(registro.txtApellido.getText());
        usuario.setNumeroTelefono(registro.txtTelefono.getText());
        usuario.setFechaNacimiento(registro.dpFechaNacimiento.getDate());
        if(registro.rbAdminRegistro.isSelected()==true){
            usuario.setTipoDeUsuario("administrador");
        }
        if(registro.rbNormalRegistro.isSelected()==true){
            usuario.setTipoDeUsuario("normal");
        }
        if(registro.rbVisualRegistro.isSelected()==true){
            usuario.setTipoDeUsuario("visual");
        }
        usuario.setNick(registro.txtNick.getText());
        usuario.setPass(registro.pwContrasena.getText());
        if(comprobarExistenciaUsuario(usuario.getNick(),usuario.getPass())==false){
            modelo.altaUsuario(usuario);
        }
    }

    /**
     * Metodo que comprueba si existe un usuario en la base de daros
     * @param nick
     * @param pass
     * @return
     */
    private boolean comprobarExistenciaUsuario(String nick,String pass) {
        boolean noExiste = false;
        for (Usuario userBBDD : modelo.getUsuarios()) {
            if (userBBDD.getNick().equals(nick)) {
                if (userBBDD.getPass().equals(pass)) {
                    noExiste = true;
                }
            }
        }
        return noExiste;
    }

    /**
     * Metodo para buscar un usuario por el nick en la BBDD
     * @param nick
     * @return
     */
    private Usuario buscarUsuarioPorNombre(String nick){
        Usuario usuarioBuscado=null;
        for (Usuario usuario : modelo.getUsuarios()) {
            if(usuario.getNick().equals(nick)){
                usuarioBuscado = usuario;
            }
        }
        return usuarioBuscado;
    }

    /**
     * Metodo que refresca todas las listas de la aplicacion
     */
    private void refrescarTodo() {
        refrescarListaUsuarios(modelo.getUsuarios());
        refrescarListaDiscograficas(modelo.getDiscograficas());
        refrescarListaCantantes(modelo.getCantantes());
        refrescarListaDiscos(modelo.getDiscos());
        refrescarListaCanciones(modelo.getCanciones());
        refrescarComboBoxDiscograficas(modelo.getDiscograficas());
        refrescarComboBoxCantantes(modelo.getCantantes());
        refrescarComboBoxDiscos(modelo.getDiscos());
    }

    /**
     * Metodo que refresca la lista usuarios
     * @param usuarios
     */
    private  void refrescarListaUsuarios(List<Usuario> usuarios){
        vista.dlmUsuariosApp.clear();
        for (Usuario usuario : usuarios) {
            vista.dlmUsuariosApp.addElement(usuario);
        }
    }

    /**
     * Metodo que refresca la lista cantantes
     * @param cantantes
     */
    private void refrescarListaCantantes(ArrayList<Cantante> cantantes) {
        vista.dlmCantantes.clear();
        for (Cantante cantante: cantantes) {
            vista.dlmCantantes.addElement(cantante);
        }
    }

    /**
     * Metodo que refresca la lista discograficas
     * @param discograficas
     */
    private void refrescarListaDiscograficas(ArrayList<Discografica> discograficas) {
        vista.dlmDiscograficas.clear();
        for (Discografica discografica: discograficas) {
            vista.dlmDiscograficas.addElement(discografica);
        }
    }

    /**
     * Metodo que refresca la lista discos
     * @param discos
     */
    private void refrescarListaDiscos(ArrayList<Disco> discos) {
        vista.dlmDiscos.clear();
        for (Disco disco: discos) {
            vista.dlmDiscos.addElement(disco);
        }
    }

    /**
     * Metodo que refresca la lista canciones
     * @param canciones
     */
    private void refrescarListaCanciones(ArrayList<Cancion> canciones) {
        vista.dlmCanciones.clear();
        for (Cancion cancion: canciones) {
            vista.dlmCanciones.addElement(cancion);
        }
    }

    /**
     * Metodo que refresca la lista cantantes contratados por una discografica
     * @param discografica
     */
    private void refrescarListaCantantesContratados(Discografica discografica) {
        vista.dlmCantantesContratados.clear();
        for (Cantante cantante : modelo.getCantantes()) {
            if(cantante.getDiscografica().getIdDiscografica()==discografica.getIdDiscografica()) {
                vista.dlmCantantesContratados.addElement(cantante);
            }
        }
    }

    /**
     * Metodo que refresca la lista de discos sacados por un cantante
     * @param cantante
     */
    private void refrescarDiscosSacados(Cantante cantante) {
        vista.dlmDiscosCantante.clear();
        for (Disco disco: modelo.getDiscos()) {
           if (disco.getCantante().getIdCantante()==cantante.getIdCantante()){
               vista.dlmDiscosCantante.addElement(disco);
           }
        }
    }

    /**
     * Metodo que refresca la lista de canciones de un disco
     * @param disco
     */
    private void refrescarCancionesDisco(Disco disco){
        vista.dlmCancionesDisco.clear();
        for (Cancion cancion : modelo.getCanciones()) {
            if(cancion.getDisco().getIdDisco()==disco.getIdDisco()){
                vista.dlmCancionesDisco.addElement(cancion);
            }
        }
    }

    /**
     * Metodo que refresca el combobox discograficas
     * @param discograficas
     */
    public void refrescarComboBoxDiscograficas(ArrayList<Discografica> discograficas){
        vista.dcbmDiscografcias.removeAllElements();
        for (Discografica discografica : discograficas) {
            vista.dcbmDiscografcias.addElement(discografica);
        }
    }

    /**
     * Metodo que refresca el combobox cantantes
     * @param cantantes
     */
    public void refrescarComboBoxCantantes (ArrayList<Cantante> cantantes){
        vista.dcbmCantantes.removeAllElements();
        for (Cantante cantante : cantantes) {
            vista.dcbmCantantes.addElement(cantante);
        }
    }

    /**
     * Metodo que refresca el combobox discos
     * @param discos
     */
    private void refrescarComboBoxDiscos(ArrayList<Disco> discos) {
        vista.dcbmDiscos.removeAllElements();
        for (Disco disco : discos) {
            vista.dcbmDiscos.addElement(disco);
        }
    }

    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields
     */
    private void limpiarCamposDiscografica(){
        vista.txtNombreDiscografica.setText("");
        vista.txtDireccion.setText("");
        vista.txtCodigoPostal.setText("");
        vista.txtCif.setText("");
        vista.txtTelefonoDiscografica.setText("");
        vista.txtCorreoElectronico.setText("");
        vista.txtNombreDirectorDiscografica.setText("");
    }

    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields
     */
    private void limpiarCamposCantantes(){
        vista.txtNombreCantante.setText("");
        vista.dpFechaNacimientoCantante.setDate(null);
        vista.txtNacionalidad.setText("");
        vista.txtPremios.setText("");
        vista.rbMasculino.setSelected(false);
        vista.rbFemenino.setSelected(false);
        vista.txtFormacion.setText("");
        vista.txtGeneroCantante.setText("");
        vista.comboBoxDiscograficas.setSelectedItem(null);
    }
    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields
     */
    private void limpiarCamposDisco(){
        vista.txtNombreDisco.setText("");
        vista.txtGeneroDisco.setText("");
        vista.dpFechaLanzamiento.setDate(null);
        vista.txtNumeroCanciones.setText("");
        vista.txtDuracionDisco.setText("");
        vista.txtFeaturings.setText("");
        vista.txtPrecio.setText("");
        vista.comboBoxCantantes.setSelectedItem(null);
    }
    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields
     */
    private void limpiarCamposCancion(){
        vista.txtTituloCancion.setText("");
        vista.txtGenero.setText("");
        vista.txtDuracion.setText("");
        vista.txtFeaturingsCancion.setText("");
        vista.txtReproducciones.setText("");
        vista.rbSi.setSelected(false);
        vista.rbNo.setSelected(false);
        vista.txtPrecioCancion.setText("");
        vista.comboBoxDiscos.setSelectedItem(null);
    }

    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields del login
     */
    private void limpiarCamposLogin(){
        login.txtUsuario.setText("");
        login.pwLogin.setText("");
    }

    /**
     * Metodo para que cuando demos de alta borre los datos introducidos en los textfields del registro
     */
    private void limpiarCamposRegistro(){
        registro.txtNombre.setText("");
        registro.txtApellido.setText("");
        registro.txtNick.setText("");
        registro.txtTelefono.setText("");
        registro.pwContrasena.setText("");
        registro.pwContrasenaComparacion.setText("");
        registro.rbNormalRegistro.setSelected(true);
    }

    /**
     * Metodo para buscar una discografica por nombre o direccion
     * @param valor
     * @param campo
     */
    private void buscarDiscografica(String valor, String campo){
        vista.dlmDiscograficas.clear();
        for(Discografica discografica : modelo.getDiscograficas()){
            if(campo.equalsIgnoreCase("nombre")) {
                if(discografica.getNombre().contains((valor))) {
                    vista.dlmDiscograficas.addElement(discografica);
                }
            } else if (campo.equalsIgnoreCase("direccion")){
                if(discografica.getDireccion().contains(valor)){
                    vista.dlmDiscograficas.addElement(discografica);
                }
            }
        }
    }
    /**
     * Metodo para buscar un cantante por nombre o nacionalidad
     * @param valor
     * @param campo
     */
    private void buscarCantante(String valor, String campo){
        vista.dlmCantantes.clear();
        for(Cantante cantante : modelo.getCantantes()){
            if(campo.equalsIgnoreCase("nombre")) {
                if(cantante.getNombre().contains((valor))) {
                    vista.dlmCantantes.addElement(cantante);
                }
            } else if (campo.equalsIgnoreCase("nacionalidad")){
                if(cantante.getNacionalidad().contains(valor)){
                    vista.dlmCantantes.addElement(cantante);
                }
            }
        }
    }
    /**
     * Metodo para buscar un disco por nombre o genero
     * @param valor
     * @param campo
     */
    private void buscarDisco(String valor, String campo){
        vista.dlmDiscos.clear();
        for(Disco disco : modelo.getDiscos()){
            if(campo.equalsIgnoreCase("nombre")) {
                if(disco.getNombre().contains((valor))) {
                    vista.dlmDiscos.addElement(disco);
                }
            } else if (campo.equalsIgnoreCase("genero")){
                if(disco.getGenero().contains(valor)){
                    vista.dlmDiscos.addElement(disco);
                }
            }
        }
    }
    /**
     * Metodo para buscar una cancion por nombre o nombre del director
     * @param valor
     * @param campo
     */
    private void buscarCancion(String valor, String campo){
        vista.dlmCanciones.clear();
        for(Cancion cancion : modelo.getCanciones()){
            if(campo.equalsIgnoreCase("nombre")) {
                if(cancion.getTitulo().contains((valor))) {
                    vista.dlmCanciones.addElement(cancion);
                }
            } else if (campo.equalsIgnoreCase("genero")){
                if(cancion.getGenero().contains(valor)){
                    vista.dlmCanciones.addElement(cancion);
                }
            }
        }
    }

    /**
     * Pone la informacion del objeto seleccionado en las diferentes listas en los campos de las vistas
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listDiscograficas) {
                Discografica discograficaSeleccionado = (Discografica) vista.listDiscograficas.getSelectedValue();
                vista.txtNombreDiscografica.setText(discograficaSeleccionado.getNombre());
                vista.txtDireccion.setText(discograficaSeleccionado.getDireccion());
                vista.txtCodigoPostal.setText(discograficaSeleccionado.getCodigoPostal());
                vista.txtCif.setText(discograficaSeleccionado.getCif());
                vista.txtTelefonoDiscografica.setText(discograficaSeleccionado.getNumeroTelefono());
                vista.txtCorreoElectronico.setText(discograficaSeleccionado.getCorreoElectronico());
                vista.txtNombreDirectorDiscografica.setText(discograficaSeleccionado.getNombreDirector());
                //Refrescamos los cantantes contratados por la discografica seleccionada
                refrescarListaCantantesContratados(discograficaSeleccionado);
            }
            if(e.getSource() == vista.listCantantes){
                Cantante cantanteSeleccionado = (Cantante) vista.listCantantes.getSelectedValue();
                vista.txtNombreCantante.setText(cantanteSeleccionado.getNombre());
                vista.dpFechaNacimientoCantante.setDate(cantanteSeleccionado.getFechaNacimiento());
                vista.txtNacionalidad.setText(cantanteSeleccionado.getNacionalidad());
                vista.txtPremios.setText(cantanteSeleccionado.getPremios());
                if(cantanteSeleccionado.getSexo().equalsIgnoreCase("Masculino")){
                    vista.rbMasculino.setSelected(true);
                }else{
                    vista.rbFemenino.setSelected(true);
                }
                vista.txtFormacion.setText(cantanteSeleccionado.getFormacion());
                vista.txtGeneroCantante.setText(cantanteSeleccionado.getGenreo());
                vista.comboBoxCantantes.setSelectedItem(cantanteSeleccionado.getDiscografica());
                //Refrescamos los discos sacados por el cantante
                refrescarDiscosSacados(cantanteSeleccionado);
            }

            if(e.getSource() == vista.listDiscos){
                Disco discoSeleccionado = (Disco) vista.listDiscos.getSelectedValue();
                vista.txtNombreDisco.setText(discoSeleccionado.getNombre());
                vista.txtGeneroDisco.setText(discoSeleccionado.getGenero());
                vista.dpFechaLanzamiento.setDate(discoSeleccionado.getFechaEstreno());
                vista.txtNumeroCanciones.setText(discoSeleccionado.getNumeroCanciones()+"");
                vista.txtDuracionDisco.setText(discoSeleccionado.getDuracion());
                vista.txtFeaturings.setText(discoSeleccionado.getFeaturings());
                vista.txtPrecio.setText(discoSeleccionado.getPrecio()+"");
                vista.comboBoxCantantes.setSelectedItem(discoSeleccionado.getCantante());
                refrescarCancionesDisco(discoSeleccionado);
            }
            if(e.getSource() == vista.listCanciones){
                Cancion cancionSeleccionada = (Cancion) vista.listCanciones.getSelectedValue();
                vista.txtTituloCancion.setText(cancionSeleccionada.getTitulo());
                vista.txtGenero.setText(cancionSeleccionada.getGenero());
                vista.txtDuracion.setText(cancionSeleccionada.getDuracion());
                vista.txtFeaturingsCancion.setText(cancionSeleccionada.getFeaturing());
                vista.txtReproducciones.setText(cancionSeleccionada.getReproducciones()+"");
                if(cancionSeleccionada.getExplicita()==true){
                    vista.rbSi.setSelected(true);
                }else{
                    vista.rbNo.setSelected(true);
                }
                vista.txtPrecioCancion.setText(cancionSeleccionada.getPrecio()+"");
                vista.comboBoxDiscos.setSelectedItem(cancionSeleccionada.getDisco());
            }

            if(e.getSource() == vista.listUsuariosApp){
                Usuario userSeleccionado = (Usuario) vista.listUsuariosApp.getSelectedValue();
                if(userSeleccionado.getTipoDeUsuario().equals("administrador")){
                    vista.rbAdmin.setSelected(true);
                }
                if(userSeleccionado.getTipoDeUsuario().equals("normal")){
                    vista.rbNormal.setSelected(true);
                }
                if(userSeleccionado.getTipoDeUsuario().equals("visual")){
                    vista.rbVisual.setSelected(true);
                }
            }
        }
    }

    /**
     *  Va actualizando los metodos de busqueda segun vamos rellenando los campos de texto de busqueda de la aplicacion
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtValorDiscograficaBuscar){
            if (vista.campoDiscografica1.isSelected() ||
                    vista.campoDiscografica2.isSelected()) {
                if (vista.txtValorDiscograficaBuscar.getText().length() >= 1) {
                    String valor = vista.txtValorDiscograficaBuscar.getText();
                    String campo;
                    if(vista.campoDiscografica1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="direccion";
                    }
                    buscarDiscografica(valor, campo);
                } else {
                    refrescarListaDiscograficas(modelo.getDiscograficas());
                }
            } else {
                if(vista.txtValorDiscograficaBuscar.getText().isEmpty()) {
                    refrescarListaDiscograficas(modelo.getDiscograficas());
                } else {
                    String valor = vista.txtValorDiscograficaBuscar.getText();
                    String campo;
                    if(vista.campoDiscografica1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="direccion";
                    }
                    buscarDiscografica(valor, campo);
                }
            }
        }

        if(e.getSource() == vista.txtValorCantanteBuscar){
            if (vista.campoCantante1.isSelected() ||
                    vista.campoCantante2.isSelected()) {
                if (vista.txtValorCantanteBuscar.getText().length() >= 1) {
                    String valor = vista.txtValorCantanteBuscar.getText();
                    String campo;
                    if(vista.campoCantante1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="nacionalidad";
                    }
                    buscarCantante(valor, campo);
                } else {
                    refrescarListaCantantes(modelo.getCantantes());
                }
            } else {
                if(vista.txtValorCantanteBuscar.getText().isEmpty()) {
                    refrescarListaCantantes(modelo.getCantantes());
                } else {
                    String valor = vista.txtValorCantanteBuscar.getText();
                    String campo;
                    if(vista.campoCantante1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="nacionalidad";
                    }
                    buscarCantante(valor, campo);
                }
            }
        }

        if(e.getSource() == vista.txtValorDiscoBuscar){
            if (vista.campoDisco1.isSelected() ||
                    vista.campoDisco2.isSelected()) {
                if (vista.txtValorDiscoBuscar.getText().length() >= 1) {
                    String valor = vista.txtValorDiscoBuscar.getText();
                    String campo;
                    if(vista.campoDisco1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="genero";
                    }
                    buscarDisco(valor, campo);
                } else {
                    refrescarListaDiscos(modelo.getDiscos());
                }
            } else {
                if(vista.txtValorDiscoBuscar.getText().isEmpty()) {
                    refrescarListaDiscos(modelo.getDiscos());
                } else {
                    String valor = vista.txtValorDiscoBuscar.getText();
                    String campo;
                    if(vista.campoDisco1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="genero";
                    }
                    buscarDisco(valor, campo);
                }
            }
        }
        if(e.getSource() == vista.txtValorCancionBuscar){
            if (vista.campoCancion1.isSelected() ||
                    vista.campoCancion2.isSelected()) {
                if (vista.txtValorCancionBuscar.getText().length() >= 1) {
                    String valor = vista.txtValorCancionBuscar.getText();
                    String campo;
                    if(vista.campoCancion1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="genero";
                    }
                    buscarCancion(valor,campo);
                } else {
                    refrescarListaCanciones(modelo.getCanciones());
                }
            } else {
                if(vista.txtValorCancionBuscar.getText().isEmpty()) {
                    refrescarListaCanciones(modelo.getCanciones());
                } else {
                    String valor = vista.txtValorCancionBuscar.getText();
                    String campo;
                    if(vista.campoCancion1.isSelected()){
                        campo="nombre";
                    }else{
                        campo="genero";
                    }
                    buscarCancion(valor,campo);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * get login
     * @return
     */
    public Login getLogin() {
        return login;
    }

    /**
     * set login
     * @param login
     */
    public void setLogin(Login login) {
        this.login = login;
    }

    /**
     * get registro
     * @return
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * set registro
     * @param registro
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    /**
     * get Vista
     * @return
     */
    public VistaMain getVista() {
        return vista;
    }

    /**
     * set vista
     * @param vista
     */
    public void setVista(VistaMain vista) {
        this.vista = vista;
    }

    /**
     * get modelo
     * @return
     */
    public Modelo getModelo() {
        return modelo;
    }

    /**
     * set modelo
     * @param modelo
     */
    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }
}
