package mvc.vistas;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Clase VistaLogin
 * Ventana en la que podemos introducir nuestro usuario y contraseña para
 * acceder al software
 */
public class Login extends JDialog {
    private JPanel panelLogin;
    public JLabel lblUsuario;
    public JLabel lblPassword;
    public JTextField txtUsuario;
    public JPasswordField pwLogin;
    public JButton btnRegistro;
    public JButton btnAcceder;
    public JButton btnSalir;

    /**
     * Constructor de Login
     */
    public Login() {
        setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logo.jpg"));
        setTitle("Login");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocation(700,100);
        setSize(600, 640);
        panelLogin = new JPanel();
        setContentPane(panelLogin);
        panelLogin.setLayout(null);


        init();
        setVisible(true);

    }

    /**
     * Crea el panel de Inicio de Sesion
     */
    private void init() {
        //Creo el panel de Login y las propiedades
        panelLogin = new JPanel();
        panelLogin.setBackground(Color.WHITE);
        panelLogin.setBounds(0, 0, 600, 600);
        getContentPane().add(panelLogin);
        panelLogin.setLayout(null);

        //Creo una etiqueta para almacenar una imgaen
        JLabel lblImagen = new JLabel("");
        lblImagen.setIcon(new ImageIcon("img/login.png"));
        lblImagen.setBounds(140, 10, 500, 300);
        panelLogin.add(lblImagen);

        //Creo una para la letra de los botones y etiquetas de la ventana login
        Font fuente = new Font("Fuente",Font.BOLD,11);

        //Usuario etiqueta y campo de texto
        lblUsuario = new JLabel();
        lblUsuario.setFont(fuente);
        lblUsuario.setHorizontalAlignment(SwingConstants.CENTER);
        lblUsuario.setText("Usuario:");
        lblUsuario.setBounds(148,350,120,20);
        panelLogin.add(lblUsuario);

        txtUsuario = new JTextField();
        txtUsuario.setFont(fuente);
        txtUsuario.setHorizontalAlignment(SwingConstants.CENTER);
        txtUsuario.setBounds(275, 350, 120, 20);
        panelLogin.add(txtUsuario);
        txtUsuario.setColumns(10);

        //Password etiqueta y campo de texto
        lblPassword = new JLabel();
        lblPassword.setFont(fuente);
        lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
        lblPassword.setText("Password:");
        lblPassword.setBounds(155,400,120,20);
        panelLogin.add(lblPassword);

        pwLogin = new JPasswordField();
        pwLogin.setFont(fuente);
        pwLogin.setHorizontalAlignment(SwingConstants.CENTER);
        pwLogin.setBounds(275, 400, 120, 20);
        panelLogin.add(pwLogin);
        pwLogin.setColumns(10);

        //Creo el boton acceder
        btnAcceder = new JButton("acceder");
        btnAcceder.setActionCommand("acceder");
        btnAcceder.setFont(fuente);
        btnAcceder.setBounds(235, 450, 120, 23);
        panelLogin.add(btnAcceder);


        //Creo una label que informativa
        JLabel lblnoRegistrado = new JLabel("Si no estas registrado, pulsa aqu\u00ED");
        lblnoRegistrado.setFont(fuente);
        lblnoRegistrado.setBounds(200, 500, 216, 14);
        panelLogin.add(lblnoRegistrado);

        //Creo el boton de registro
        btnRegistro = new JButton("registro");
        btnRegistro.setActionCommand("registrar");
        btnRegistro.setFont(fuente);
        btnRegistro.setBounds(230, 530, 115, 23);
        panelLogin.add(btnRegistro);



       //Creo un boton para salir de la aplicacion
        btnSalir = new JButton();
        btnSalir.setActionCommand("salirLogin");
        btnSalir.setIcon(new ImageIcon("img/cruz.jpg"));
        btnSalir.setBounds(550, 6, 30, 30);
        panelLogin.add(btnSalir);
    }

}

