package mvc.vistas;
import com.github.lgooddatepicker.components.DatePicker;
import com.malloza.soundMusic.base.*;

import javax.swing.*;
import java.awt.*;


/**
 * Vista que tendra un usuario de la aplicacion
 * @author Manu
 * @since 20/11/2019
 */
public class VistaMain extends JFrame {

    //Creo la pestana y los cinco paneles que esta contendra
    public JTabbedPane pesana;
    public JPanel panelDiscograficas;
    public JPanel panelCantantes;
    public JPanel panelDiscos;
    public JPanel panelCanciones;
    public JPanel panelOpciones;
    public JPanel panelListUsuarios;

    //Declaro los DefaultListModel
    public DefaultListModel <Discografica> dlmDiscograficas;
    public DefaultListModel <Cantante> dlmCantantes;
    public DefaultListModel <Disco> dlmDiscos;
    public DefaultListModel <Cancion> dlmCanciones;
    public DefaultListModel <Cantante> dlmCantantesContratados;
    public DefaultListModel <Cancion> dlmCancionesDisco;
    public DefaultListModel <Disco> dlmDiscosCantante;
    public DefaultListModel <Usuario> dlmUsuariosApp;

    //Declaro Elementos de la pesteña Discograficas----------------
    public JLabel lblDiscograficas;
    public JPanel panelListaDiscograficas;
    public JPanel panelCantesContratados;
    public JList listDiscograficas;
    public JList listCantantesContratados;

    public JLabel lblListaCantantes;
    public JLabel lblNombreDiscografica;
    public JLabel lblDireccion;
    public JLabel lblCodigoPostal;
    public JLabel lblCif;
    public JLabel lblTelefonoDiscografica;
    public JLabel lblCorreoElectronico;
    public JLabel lblNombreDirectorDiscografica;

    public JLabel lblFotoDiscografica;

    public JTextField txtNombreDiscografica;
    public JTextField txtDireccion;
    public JTextField txtCodigoPostal;
    public JTextField txtCif;
    public JTextField txtTelefonoDiscografica;
    public JTextField txtCorreoElectronico;
    public JTextField txtNombreDirectorDiscografica;

    public JButton btnAnadirDiscografica;
    public JButton btnModificarDiscografica;
    public JButton btnEliminarDiscografica;


    //boton y textField para buscar
    public JLabel lb1CampoDiscograficaBuscar;
    ButtonGroup grupoBuscarDiscografica;
    public JRadioButton campoDiscografica1;
    public JRadioButton campoDiscografica2;
    public JLabel lblValorDiscograficaBuscar;
    public JTextField txtValorDiscograficaBuscar;


    //Declaro Elementos de la pesteña cantantes----------------
    public JLabel lblCantantes;
    public JPanel panelListaCantantes;
    public JPanel panelDiscosCantante;

    public JList listCantantes;
    public JList listDiscosCantante;

    public JComboBox comboBoxDiscograficas;
    public DefaultComboBoxModel dcbmDiscografcias;

    ButtonGroup grupoCheckBoxSexo;

    public JLabel lblListaDiscosCantante;
    public JLabel lblNombreCantante;
    public JLabel lblFechaNacimiento;
    public JLabel lblNacionalidad;
    public JLabel lblPremios;
    public JLabel lblGeneroCantante;
    public JLabel lblFoto;
    public JLabel lblSexo;
    public JLabel lblFormacion;
    public JLabel lblDiscografica;

    public JTextField txtNombreCantante;
    public DatePicker dpFechaNacimientoCantante;
    public JTextField txtNacionalidad;
    public JTextField txtPremios;
    public JTextField txtGeneroCantante;

    public JLabel lblFotoCantante;

    //Sexo
    public JRadioButton rbMasculino;
    public JRadioButton rbFemenino;
    public JTextField txtFormacion;



    public JButton btnAnadirCantante;
    public JButton btnModificarCantante;
    public JButton btnEliminarCantante;

    //boton y textField para buscar
    public JLabel lblCampoCantanteBuscar;
    ButtonGroup grupoBuscarCantante;
    public JRadioButton campoCantante1;
    public JRadioButton campoCantante2;
    public JLabel lblValorCantanteBuscar;
    public JTextField txtValorCantanteBuscar;

    //Declaro Elementos de la pestaña Discos--------------
    public JLabel lblDiscos;
    public JPanel panelListaDiscos;
    public JPanel panelCancionesDisco;
    public JList listDiscos;
    public JList listCancionesDisco;
    public JComboBox comboBoxCantantes;
    public DefaultComboBoxModel <Cantante> dcbmCantantes;

    public JLabel lblCancionesDisco;
    public JLabel lblNombreDisco;
    public JLabel lblGeneroDisco;
    public JLabel lblFechaLanzamiento;
    public JLabel lblNumeroCanciones;
    public JLabel lblDuracionDisco;
    public JLabel lblFeaturings;
    public JLabel lblPrecio;
    public JLabel lblCantante;

    public JTextField txtNombreDisco;
    public JTextField txtGeneroDisco;
    public DatePicker dpFechaLanzamiento;
    public JTextField txtNumeroCanciones;
    public JTextField txtDuracionDisco;
    public JTextField txtFeaturings;
    public JTextField txtPrecio;

    public JLabel lblFotoCaratula;

    public JButton btnAnadirDisco;
    public JButton btnModificarDisco;
    public JButton btnEliminarDisco;

    //boton y textField para buscar
    public JLabel lblCampoDiscoBuscar;
    ButtonGroup grupoBuscarDisco;
    public JRadioButton campoDisco1;
    public JRadioButton campoDisco2;
    public JLabel lblValorDiscoBuscar;
    public JTextField txtValorDiscoBuscar;

    //Declaro Elementos de la pestaña canciones----------------
    public JLabel lblCanciones;
    public JPanel panelListaCanciones;
    public JList listCanciones;

    public JComboBox comboBoxDiscos;
    public DefaultComboBoxModel dcbmDiscos;

    ButtonGroup grupoCheckBoxExplicita;

    public JLabel lblTituloCancion;
    public JLabel lblGenero;
    public JLabel lblDuracionCancion;
    public JLabel lblFeaturingsCancion;
    public JLabel lblReproducciones;
    public JLabel lblExplicita;
    public JLabel lblPrecioCancion;
    public JLabel lblDisco;

    public JLabel lblFotoCancion;

    public JTextField txtTituloCancion;
    public JTextField txtGenero;
    public JTextField txtDuracion;
    public JTextField txtFeaturingsCancion;
    public JTextField txtReproducciones;
    public JRadioButton rbSi;
    public JRadioButton rbNo;
    public JTextField txtPrecioCancion;

    public JButton btnAnadirCancion;
    public JButton btnModificarCancion;
    public JButton btnEliminarCancion;

    public JLabel lblCampoCancionBuscar;
    ButtonGroup grupoBuscarCancion;
    public JRadioButton campoCancion1;
    public JRadioButton campoCancion2;
    public JLabel lblValorCancionBuscar;
    public JTextField txtValorCancionBuscar;


    //Creo todos los elementos de la pestaña de opciones
    //Label distintas opciones del programa
    public JLabel lblTemaAplicacion;
    public JLabel lblInformes;

    //Temas de la aplicacion
    public JRadioButton rbTemaPredeterminado;
    public JRadioButton rbRap;
    public JRadioButton rbPop;

    //Botones de los informes
    public JButton btnInformesCantantes;
    public JButton btnInformeReproduciones;

    //Boton para cambiear de usuario
    public JButton btnCambiatUsuario;

    //Declaro los elementos de la pestaña de control de usuarios
    public JList listUsuariosApp;

    public ButtonGroup tipoUsuarioGrupo;

    public JLabel lblUsuarios;
    public JLabel lblTipoUsuario;

    public JRadioButton rbAdmin;
    public JRadioButton rbNormal;
    public JRadioButton rbVisual;

    public JButton btnModificarUsuario;
    public JButton btnEliminarUsuario;

    /**
     * Constructor de la vistaUsuario
     */
    public VistaMain() {
        init();
    }

    /**
     * Metodo para crear las pestañas de la ventana y llamaos a los metodos de configuracion de cada una da ellas
     */
    public void init() {
        //Creo la pestaña y los tres paneles que la componen
        pesana = new JTabbedPane();
        panelDiscograficas = new JPanel();
        panelCantantes = new JPanel();
        panelDiscos = new JPanel();
        panelCanciones = new JPanel();
        panelOpciones = new JPanel();

        //Llamos a los metodos que configuran la ventana y cada una de las pestañas de la misma
        configurarPestanaDiscograficas();
        configurarPestanaCantantes();
        configurarPestanaDiscos();
        configurarPestanaCanciones();
        configurarPestanaOpciones();
        configuracionVentana();

        //Añado los paneles a la pestaña
        pesana.add(panelDiscograficas,"Discograficas");
        pesana.add(panelCantantes,"Cantantes");
        pesana.add(panelDiscos,"Discos");
        pesana.add(panelCanciones,"Cancion");
        pesana.add(panelOpciones,"Opciones");

        inicializarLista();
    }


    /**
     * Inicia la lista para que contenga elementos desde el inicio
     */
    public void inicializarLista() {
        //Creamos dos peliculas  las añadimos a dlm

    }

    /**
     * Metodo que configura la ventatana
     */
    public void configuracionVentana(){
        this.getContentPane().add(pesana);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setSize(1800,1200);
        setTitle("Sound Music");
        setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logo.jpg"));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Metodo que configura los elemntos y la distribucion del panel Discograficas
     */
    public void configurarPestanaDiscograficas() {
        panelDiscograficas.setBackground(Color.WHITE);
        panelDiscograficas.setBounds(10,10,200,200);
        //No le damos un BorderLayout al panel
        panelDiscograficas.setLayout(null);

        //Creo un label para la lista de Discograficas
        lblDiscograficas = new JLabel("Lista de la discograficas");
        lblDiscograficas.setBounds(1000,75,200,12);
        panelDiscograficas.add(lblDiscograficas);

        //Creo un JList y defaultListModel los unimos y lo mento en el panel discograficas
        panelListaDiscograficas = new JPanel();
        listDiscograficas = new JList<>();
        dlmDiscograficas = new DefaultListModel<>();
        listDiscograficas.setModel(dlmDiscograficas);
        panelListaDiscograficas.add(listDiscograficas);
        panelListaDiscograficas.setBounds(1000,100,900,700);
        panelDiscograficas.add(panelListaDiscograficas);

        //Label y campo de texto de el nombre de la discografica
        lblNombreDiscografica = new JLabel("Nombre Discografica:");
        lblNombreDiscografica.setBounds(30,100,150,12);
        panelDiscograficas.add(lblNombreDiscografica);

        txtNombreDiscografica = new JTextField();
        txtNombreDiscografica.setBounds(200,98,200,18);
        panelDiscograficas.add(txtNombreDiscografica);

        //Label y campo de texto de la direccion de la discografica
        lblDireccion = new JLabel("Direccion:");
        lblDireccion.setBounds(30,130,150,12);
        panelDiscograficas.add(lblDireccion);

        txtDireccion = new JTextField();
        txtDireccion.setBounds(200,128,200,18);
        panelDiscograficas.add(txtDireccion);

        //Label y campo de texto del codigo postal de la discografica
        lblCodigoPostal = new JLabel("Codigo Postal:");
        lblCodigoPostal.setBounds(30,160,150,18);
        panelDiscograficas.add(lblCodigoPostal);

        txtCodigoPostal = new JTextField();
        txtCodigoPostal.setBounds(200,158,200,18);
        panelDiscograficas.add(txtCodigoPostal);

        //Label y campo de texto de el Cif de la discografica
        lblCif = new JLabel("Cif:");
        lblCif.setBounds(30,190,150,12);
        panelDiscograficas.add(lblCif);

        txtCif = new JTextField();
        txtCif.setBounds(200,188,200,18);
        panelDiscograficas.add(txtCif);

        //Label y campo de texto del telfono de la discografica
        lblTelefonoDiscografica = new JLabel("Telefono:");
        lblTelefonoDiscografica.setBounds(30,220,150,12);
        panelDiscograficas.add(lblTelefonoDiscografica);

        txtTelefonoDiscografica = new JTextField();
        txtTelefonoDiscografica.setBounds(200,218,200,18);
        panelDiscograficas.add(txtTelefonoDiscografica);

        //Label y campo de texto del telfono de la discografica
        lblCorreoElectronico = new JLabel("Correo Electronico:");
        lblCorreoElectronico.setBounds(30,250,150,12);
        panelDiscograficas.add(lblCorreoElectronico);

        txtCorreoElectronico = new JTextField();
        txtCorreoElectronico.setBounds(200,248,200,18);
        panelDiscograficas.add(txtCorreoElectronico);

        //Label y campo de texto del telfono de la discografica
        lblNombreDirectorDiscografica = new JLabel("Nombre Dueño:");
        lblNombreDirectorDiscografica.setBounds(30,280,150,12);
        panelDiscograficas.add(lblNombreDirectorDiscografica);

        txtNombreDirectorDiscografica = new JTextField();
        txtNombreDirectorDiscografica.setBounds(200,278,200,18);
        panelDiscograficas.add(txtNombreDirectorDiscografica);

        //Label y campo de texto del telfono de la discografica
        lblListaCantantes = new JLabel("Cantantes contratados por la discografica:");
        lblListaCantantes.setBounds(30,310,300,18);
        panelDiscograficas.add(lblListaCantantes);

        lblFotoDiscografica = new JLabel("");
        lblFotoDiscografica.setIcon(new ImageIcon("img/discografica.jpg"));
        lblFotoDiscografica.setBounds(650, 100, 160, 160);
        panelDiscograficas.add(lblFotoDiscografica);

        //Panel y lista de los cantantes contratados
        panelCantesContratados = new JPanel();
        listCantantesContratados = new JList();
        dlmCantantesContratados = new DefaultListModel<>();
        listCantantesContratados.setModel(dlmCantantesContratados);
        panelCantesContratados.add(listCantantesContratados);
        panelCantesContratados.setBounds(200,330,700,472);
        panelDiscograficas.add(panelCantesContratados);

        //Botones de dar de alta, modificar y eliminar
        //Alta
        btnAnadirDiscografica = new JButton(" Alta ");
        btnAnadirDiscografica.setActionCommand("altaDiscografica");
        btnAnadirDiscografica.setBounds(30,820,140,20);
        panelDiscograficas.add(btnAnadirDiscografica);
        //Modificar
        btnModificarDiscografica = new JButton(" Modificar ");
        btnModificarDiscografica.setActionCommand("modificarDiscografica");
        btnModificarDiscografica.setBounds(180,820,140,20);
        panelDiscograficas.add(btnModificarDiscografica);
        //Eliminar
        btnEliminarDiscografica = new JButton(" Eliminar ");
        btnEliminarDiscografica.setActionCommand("eliminarDiscografica");
        btnEliminarDiscografica.setBounds(330,820,140,20);
        panelDiscograficas.add(btnEliminarDiscografica);

        //Labels y campso de texto para buscar en la lista de discograficas
        lb1CampoDiscograficaBuscar = new JLabel("Selecciona campo busqueda");
        lb1CampoDiscograficaBuscar.setBounds(1000,900,200,18);
        panelDiscograficas.add(lb1CampoDiscograficaBuscar);

        grupoBuscarDiscografica = new ButtonGroup();
        campoDiscografica1 = new JRadioButton("Nombre");
        campoDiscografica2 = new JRadioButton("Direccion");
        campoDiscografica1.setBounds(1200,870,200,18);
        campoDiscografica2.setBounds(1200,930,200,18);
        grupoBuscarDiscografica.add(campoDiscografica1);
        grupoBuscarDiscografica.add(campoDiscografica2);
        panelDiscograficas.add(campoDiscografica1);
        panelDiscograficas.add(campoDiscografica2);

        lblValorDiscograficaBuscar = new JLabel("Valor a buscar");
        lblValorDiscograficaBuscar.setBounds(1500,900,200,18);
        panelDiscograficas.add(lblValorDiscograficaBuscar);

        txtValorDiscograficaBuscar = new JTextField();
        txtValorDiscograficaBuscar.setBounds(1600,900,200,18);
        panelDiscograficas.add(txtValorDiscograficaBuscar);
    }

    /**
     * Metodo que configura los elemntos y la distribucion del panel Cantantes
     */
    public void configurarPestanaCantantes() {
        panelCantantes.setBackground(Color.WHITE);
        panelCantantes.setBounds(10,10,200,200);
        //No le damos un BorderLayout al panel
        panelCantantes.setLayout(null);

        //Creo un label para la lista de Cantantes
        lblCantantes = new JLabel("Lista de la cantantes");
        lblCantantes.setBounds(1000,75,200,12);
        panelCantantes.add(lblCantantes);


        panelListaCantantes= new JPanel();
        listCantantes = new JList<>();
        dlmCantantes = new DefaultListModel<>();
        listCantantes.setModel(dlmCantantes);
        panelListaCantantes.add(listCantantes);
        panelListaCantantes.setBounds(1000,100,900,700);
        panelCantantes.add(panelListaCantantes);

        //Label y campo de texto de el nombre del cantante
        lblNombreCantante = new JLabel("Nombre Cantante:");
        lblNombreCantante.setBounds(30,100,150,12);
        panelCantantes.add(lblNombreCantante);

        txtNombreCantante  = new JTextField();
        txtNombreCantante.setBounds(200,98,200,18);
        panelCantantes.add(txtNombreCantante);

        //Label y campo de texto de la fecha de nacimiento
        lblFechaNacimiento = new JLabel("Fecha de Nacimiento:");
        lblFechaNacimiento.setBounds(30,130,150,12);
        panelCantantes.add(lblFechaNacimiento);

        dpFechaNacimientoCantante = new DatePicker();
        dpFechaNacimientoCantante.setBounds(200,128,200,25);
        panelCantantes.add(dpFechaNacimientoCantante);

        //Label y campo de texto de la nacionalidad del cantante
        lblNacionalidad = new JLabel("Nacionalidad:");
        lblNacionalidad.setBounds(30,160,150,18);
        panelCantantes.add(lblNacionalidad);

        txtNacionalidad = new JTextField();
        txtNacionalidad.setBounds(200,158,200,18);
        panelCantantes.add(txtNacionalidad);

        //Label y campo de texto de los premios del cantante
        lblPremios = new JLabel("Premios:");
        lblPremios.setBounds(30,190,150,12);
        panelCantantes.add(lblPremios);

        txtPremios = new JTextField();
        txtPremios.setBounds(200,188,200,18);
        panelCantantes.add(txtPremios);

        lblFotoCantante = new JLabel("");
        lblFotoCantante.setIcon(new ImageIcon("img/cantante.jpg"));
        lblFotoCantante.setBounds(650, 100, 160, 200);
        panelCantantes.add(lblFotoCantante);

        //Label y check box del atributo sexo
        lblSexo= new JLabel("Sexo:");
        lblSexo.setBounds(30,220,150,12);
        panelCantantes.add(lblSexo);

        grupoCheckBoxSexo = new ButtonGroup();
        rbMasculino = new JRadioButton("Hombre");
        rbFemenino = new JRadioButton("Femenino");
        grupoCheckBoxSexo.add(rbMasculino);
        grupoCheckBoxSexo.add(rbFemenino);
        rbMasculino.setSelected(true);
        rbMasculino.setBounds(200,220,150,12);
        rbFemenino.setBounds(400,220,150,12);
        panelCantantes.add(rbMasculino);
        panelCantantes.add(rbFemenino);

        //Label y campo de texo del atributo formacion
        lblFormacion = new JLabel("Formacion:");
        lblFormacion.setBounds(30,250,150,12);
        panelCantantes.add(lblFormacion);

        txtFormacion = new JTextField();
        txtFormacion.setBounds(200,249,200,18);
        panelCantantes.add(txtFormacion);

        lblGeneroCantante = new JLabel("Genero musical:");
        lblGeneroCantante.setBounds(30,270,150,12);
        panelCantantes.add(lblGeneroCantante);

        txtGeneroCantante = new JTextField();
        txtGeneroCantante.setBounds(200,268,200,18);
        panelCantantes.add(txtGeneroCantante);

        //Combobox de Discograficas
        lblDiscografica = new JLabel("Discografica");
        lblDiscografica.setBounds(30,300,200,12);
        panelCantantes.add(lblDiscografica);

        comboBoxDiscograficas = new JComboBox();
        dcbmDiscografcias = new DefaultComboBoxModel();
        comboBoxDiscograficas.setModel(dcbmDiscografcias);
        comboBoxDiscograficas.setBounds(200,300,400,25);
        panelCantantes.add(comboBoxDiscograficas);

        //Panel y lista de los discos cantante
        panelDiscosCantante = new JPanel();
        listDiscosCantante = new JList();
        dlmDiscosCantante = new DefaultListModel<>();
        listDiscosCantante.setModel(dlmDiscosCantante);
        panelDiscosCantante.add(listDiscosCantante);
        panelDiscosCantante.setBounds(200,330,700,475);
        panelCantantes.add(panelDiscosCantante);

        //Label de lista de discos cantante
        lblListaDiscosCantante = new JLabel("Discos sacados:");
        lblListaDiscosCantante.setBounds(30,330,300,18);
        panelCantantes.add(lblListaDiscosCantante);

        //Botones de dar de alta, modificar y eliminar
        //Alta
        btnAnadirCantante = new JButton(" Alta ");
        btnAnadirCantante.setActionCommand("altaCantante");
        btnAnadirCantante.setBounds(30,820,140,20);
        panelCantantes.add(btnAnadirCantante);
        //Modificar
        btnModificarCantante = new JButton(" Modificar ");
        btnModificarCantante.setActionCommand("modificarCantante");
        btnModificarCantante.setBounds(180,820,140,20);
        panelCantantes.add(btnModificarCantante);
        //Eliminar
        btnEliminarCantante = new JButton(" Eliminar ");
        btnEliminarCantante.setActionCommand("eliminarCantante");
        btnEliminarCantante.setBounds(330,820,140,20);
        panelCantantes.add(btnEliminarCantante);

        //Labels y campso de texto para buscar en la lista de discograficas
        lblCampoCantanteBuscar = new JLabel("Selecciona campo busqueda");
        lblCampoCantanteBuscar.setBounds(1000,900,200,18);
        panelCantantes.add(lblCampoCantanteBuscar);

        grupoBuscarCantante = new ButtonGroup();
        campoCantante1 = new JRadioButton("Nombre");
        campoCantante2 = new JRadioButton("Nacionalidad");
        campoCantante1.setBounds(1200,870,200,18);
        campoCantante2.setBounds(1200,930,200,18);
        grupoBuscarCantante.add(campoCantante1);
        grupoBuscarCantante.add(campoCantante2);
        panelCantantes.add(campoCantante1);
        panelCantantes.add(campoCantante2);

        lblValorCantanteBuscar = new JLabel("Valor a buscar");
        lblValorCantanteBuscar.setBounds(1500,900,200,18);
        panelCantantes.add(lblValorCantanteBuscar);

        txtValorCantanteBuscar = new JTextField();
        txtValorCantanteBuscar.setBounds(1600,900,200,18);
        panelCantantes.add(txtValorCantanteBuscar);
    }

    /**
     * Metodo que configura los elemntos y la distribucion del panel Discos
     */
    public void configurarPestanaDiscos(){
        panelDiscos.setBackground(Color.WHITE);
        panelDiscos.setLayout(null);

        //Creo un label para la lista de Discos
        lblDiscos = new JLabel("Lista de la discos");
        lblDiscos.setBounds(1000,75,200,12);
        panelDiscos.add(lblDiscos);

        //Creo un JList y defaultListModel los unimos y meto la lista en el norte del panel Peliculas
        panelListaDiscos = new JPanel();
        listDiscos = new JList<>();
        dlmDiscos = new DefaultListModel<>();
        listDiscos.setModel(dlmDiscos);
        panelListaDiscos.add(listDiscos);
        panelListaDiscos.setBounds(1000,100,900,700);
        panelDiscos.add(panelListaDiscos);

        //Titulo del disco
        lblNombreDisco = new JLabel("Nombre Disco:");
        lblNombreDisco.setBounds(30,100,150,12);
        panelDiscos.add(lblNombreDisco);

        txtNombreDisco = new JTextField();
        txtNombreDisco.setBounds(200,98,200,18);
        panelDiscos.add(txtNombreDisco);

        //Genero del disco
        lblGeneroDisco = new JLabel("Genero");
        lblGeneroDisco.setBounds(30,130, 150,12);
        panelDiscos.add(lblGeneroDisco);

        txtGeneroDisco = new JTextField();
        txtGeneroDisco.setBounds(200,128,200,18);
        panelDiscos.add(txtGeneroDisco);

        //Fecha de lanzamiento del disco
        lblFechaLanzamiento = new JLabel("Fecha Lanzamiento:");
        lblFechaLanzamiento.setBounds(30,160,150,12);
        panelDiscos.add(lblFechaLanzamiento);

        dpFechaLanzamiento = new DatePicker();
        dpFechaLanzamiento.setBounds(200,155,200,24);
        panelDiscos.add(dpFechaLanzamiento);

        //Numero de canciones del disco
        lblNumeroCanciones = new JLabel("Numero de canciones:");
        lblNumeroCanciones.setBounds(30,190,150,18);
        panelDiscos.add(lblNumeroCanciones);

        txtNumeroCanciones = new JTextField();
        txtNumeroCanciones.setBounds(200,189,200,18);
        panelDiscos.add(txtNumeroCanciones);

       //Duracion del disco
        lblDuracionDisco = new JLabel("Duracion disco:");
        lblDuracionDisco.setBounds(30,220,150,12);
        panelDiscos.add(lblDuracionDisco);

        txtDuracionDisco = new JTextField();
        txtDuracionDisco.setBounds(200,218,200,18);
        panelDiscos.add(txtDuracionDisco);

        //Featurings del disco
        lblFeaturings = new JLabel("Featurings:");
        lblFeaturings.setBounds(30,240,150,12);
        panelDiscos.add(lblFeaturings);

        txtFeaturings = new JTextField();
        txtFeaturings.setBounds(200,238,200,18);
        panelDiscos.add(txtFeaturings);

        //Precio disco
        lblPrecio = new JLabel("Precio:");
        lblPrecio.setBounds(30,260,150,12);
        panelDiscos.add(lblPrecio);

        txtPrecio = new JTextField();
        txtPrecio.setBounds(200,258,200,18);
        panelDiscos.add(txtPrecio);

        //Cantante del disco
        lblCantante = new JLabel("Cantante:");
        lblCantante.setBounds(30,280,150,12);
        panelDiscos.add(lblCantante);

        comboBoxCantantes = new JComboBox();
        dcbmCantantes = new DefaultComboBoxModel<>();
        comboBoxCantantes.setModel(dcbmCantantes);
        comboBoxCantantes.setBounds(200,280,400,25);
        panelDiscos.add(comboBoxCantantes);

        //Label con imagenes
        lblFotoCaratula = new JLabel("");
        lblFotoCaratula.setIcon(new ImageIcon("img/disco.jpg"));
        lblFotoCaratula.setBounds(650, 100, 160, 160);
        panelDiscos.add(lblFotoCaratula);

        //label de la lista de canciones del disco
        lblCancionesDisco = new JLabel("Canciones del disco:");
        lblCancionesDisco.setBounds(30,330,300,18);
        panelDiscos.add(lblCancionesDisco);

        //Panel y lista de las canciones del disco
        panelCancionesDisco = new JPanel();
        listCancionesDisco = new JList();
        dlmCancionesDisco = new DefaultListModel<>();
        listCancionesDisco.setModel(dlmCancionesDisco);
        panelCancionesDisco.add(listCancionesDisco);
        panelCancionesDisco.setBounds(200,330,700,472);
        panelDiscos.add(panelCancionesDisco);

        //Botones de dar de alta, modificar y eliminar la lista de discos
        //Alta
        btnAnadirDisco = new JButton(" Alta ");
        btnAnadirDisco.setActionCommand("altaDisco");
        btnAnadirDisco.setBounds(30,820,140,20);
        panelDiscos.add(btnAnadirDisco);
        //Modificar
        btnModificarDisco = new JButton(" Modificar ");
        btnModificarDisco.setActionCommand("modificarDisco");
        btnModificarDisco.setBounds(180,820,140,20);
        panelDiscos.add(btnModificarDisco);
        //Eliminar
        btnEliminarDisco = new JButton(" Eliminar ");
        btnEliminarDisco.setActionCommand("eliminarDisco");
        btnEliminarDisco.setBounds(330,820,140,20);
        panelDiscos.add(btnEliminarDisco);

        //Labels y campso de texto para buscar en la lista de discograficas
        lblCampoDiscoBuscar = new JLabel("Selecciona campo busqueda");
        lblCampoDiscoBuscar.setBounds(1000,900,200,18);
        panelDiscos.add(lblCampoDiscoBuscar);

        grupoBuscarDisco = new ButtonGroup();
        campoDisco1 = new JRadioButton("Nombre");
        campoDisco2 = new JRadioButton("Genero");
        campoDisco1.setBounds(1200,870,200,18);
        campoDisco2.setBounds(1200,930,200,18);
        grupoBuscarDisco.add(campoDisco1);
        grupoBuscarDisco.add(campoDisco2);
        panelDiscos.add(campoDisco1);
        panelDiscos.add(campoDisco2);

        lblValorDiscoBuscar = new JLabel("Valor a buscar");
        lblValorDiscoBuscar.setBounds(1500,900,200,18);
        panelDiscos.add(lblValorDiscoBuscar);

        txtValorDiscoBuscar = new JTextField();
        txtValorDiscoBuscar.setBounds(1600,900,200,18);
        panelDiscos.add(txtValorDiscoBuscar);
    }

    /**
     * Metodo que configura los elemntos y la distribucion del panel Cancion
     */
    public void configurarPestanaCanciones() {
        panelCanciones.setBackground(Color.WHITE);
        panelCanciones.setLayout(null);

        //Creo un label para la lista de Cancion
        lblCanciones = new JLabel("Lista de la canciones");
        lblCanciones.setBounds(1000,75,200,12);
        panelCanciones.add(lblCanciones);

        //Creo un JList y defaultListModel los unimos y meto la lista en el norte del panel Peliculas
        panelListaCanciones = new JPanel();
        listCanciones = new JList<>();
        dlmCanciones = new DefaultListModel<>();
        listCanciones.setModel(dlmCanciones);
        panelListaCanciones.add(listCanciones);
        panelListaCanciones.setBounds(1000, 100, 900, 700);
        panelCanciones.add(panelListaCanciones);

        //Label y campo de texto de el nombre de la discografica
        lblTituloCancion = new JLabel("Titulo:");
        lblTituloCancion.setBounds(30, 100, 150, 12);
        panelCanciones.add(lblTituloCancion);

        txtTituloCancion = new JTextField();
        txtTituloCancion.setBounds(200, 98, 200, 18);
        panelCanciones.add(txtTituloCancion);

        //Label y campo de texto de la direccion de la discografica
        lblGenero = new JLabel("Genero:");
        lblGenero.setBounds(30, 130, 150, 12);
        panelCanciones.add(lblGenero);

        txtGenero = new JTextField();
        txtGenero.setBounds(200, 128, 200, 18);
        panelCanciones.add(txtGenero);

        //Label y campo de texto del codigo postal de la discografica
        lblDuracionCancion = new JLabel("Duracion:");
        lblDuracionCancion.setBounds(30, 160, 150, 18);
        panelCanciones.add(lblDuracionCancion);

        txtDuracion = new JTextField();
        txtDuracion.setBounds(200, 158, 200, 18);
        panelCanciones.add(txtDuracion);

        //Label y campo de texto de el Cif de la discografica
        lblFeaturingsCancion = new JLabel("Featurings cancion:");
        lblFeaturingsCancion.setBounds(30, 190, 150, 12);
        panelCanciones.add(lblFeaturingsCancion);

        txtFeaturingsCancion = new JTextField();
        txtFeaturingsCancion.setBounds(200, 188, 200, 18);
        panelCanciones.add(txtFeaturingsCancion);

        //Label y campo de texto de Reproducciones
        lblReproducciones = new JLabel("Reproducciones:");
        lblReproducciones.setBounds(30, 220, 150, 12);
        panelCanciones.add(lblReproducciones);

        txtReproducciones = new JTextField();
        txtReproducciones.setBounds(200, 218, 200, 18);
        panelCanciones.add(txtReproducciones);

        //Label y campo de texto del telfono de la discografica
        lblExplicita = new JLabel("Explicita:");
        lblExplicita.setBounds(30, 250, 150, 12);
        panelCanciones.add(lblExplicita);

        //Radio buttons de si es explicita
        rbSi = new JRadioButton("SI");
        rbNo = new JRadioButton("NO");
        grupoCheckBoxExplicita = new ButtonGroup();
        grupoCheckBoxExplicita.add(rbSi);
        grupoCheckBoxExplicita.add(rbNo);
        rbNo.setSelected(true);
        rbSi.setBounds(200,250,150,16);
        rbNo.setBounds(400,250,150,16);
        panelCanciones.add(rbSi);
        panelCanciones.add(rbNo);

        //Label y campo de texto del telfono de la discografica
        lblPrecioCancion = new JLabel("Precio canción:");
        lblPrecioCancion.setBounds(30, 280, 150, 12);
        panelCanciones.add(lblPrecioCancion);

        txtPrecioCancion = new JTextField();
        txtPrecioCancion.setBounds(200, 278, 200, 18);
        panelCanciones.add(txtPrecioCancion);

        //Disco
        lblDisco = new JLabel("Disco:");
        lblDisco.setBounds(30, 310, 300, 18);
        panelCanciones.add(lblDisco);

        //ComboBox Discos
        //Creo el ComboxBox
        comboBoxDiscos = new JComboBox();
        dcbmDiscos = new DefaultComboBoxModel<>();
        comboBoxDiscos.setModel(dcbmDiscos);
        comboBoxDiscos.setBounds(200,308,400,25);
        panelCanciones.add(comboBoxDiscos);

        //Label con imagenes
        lblFotoCancion = new JLabel("");
        lblFotoCancion.setIcon(new ImageIcon("img/cancion.png"));
        lblFotoCancion.setBounds(650, 100, 160, 160);
        panelCanciones.add(lblFotoCancion);

        //Botones de dar de alta, modificar y eliminar
        //Alta
        btnAnadirCancion = new JButton(" Alta ");
        btnAnadirCancion.setActionCommand("altaCancion");
        btnAnadirCancion.setBounds(30, 820, 140, 20);
        panelCanciones.add(btnAnadirCancion);
        //modificar
        btnModificarCancion = new JButton(" Modificar ");
        btnModificarCancion.setActionCommand("modificarCancion");
        btnModificarCancion.setBounds(180, 820, 140, 20);
        panelCanciones.add(btnModificarCancion);
        //Eliminar
        btnEliminarCancion = new JButton(" Eliminar ");
        btnEliminarCancion.setActionCommand("eliminarCancion");
        btnEliminarCancion.setBounds(330, 820, 140, 20);
        panelCanciones.add(btnEliminarCancion);

        //Labels y campso de texto para buscar en la lista de discograficas
        lblCampoCancionBuscar = new JLabel("Selecciona campo busqueda");
        lblCampoCancionBuscar.setBounds(1000,900,200,18);
        panelCanciones.add(lblCampoCancionBuscar);

        grupoBuscarCancion = new ButtonGroup();
        campoCancion1 = new JRadioButton("Titulo");
        campoCancion2 = new JRadioButton("Genero");
        campoCancion1.setBounds(1200,870,200,18);
        campoCancion2.setBounds(1200,930,200,18);
        grupoBuscarCancion.add(campoCancion1);
        grupoBuscarCancion.add(campoCancion2);
        panelCanciones.add(campoCancion1);
        panelCanciones.add(campoCancion2);

        lblValorCancionBuscar = new JLabel("Valor a buscar");
        lblValorCancionBuscar.setBounds(1500,900,200,18);
        panelCanciones.add(lblValorCancionBuscar);

        txtValorCancionBuscar = new JTextField();
        txtValorCancionBuscar.setBounds(1600,900,200,18);
        panelCanciones.add(txtValorCancionBuscar);
    }

    /**
     * Metodo que configura los elemntos y la distribucion del panel opciones
     */
    public void configurarPestanaOpciones() {
        panelOpciones.setBackground(Color.WHITE);
        panelOpciones.setLayout(null);

        //Labels del panel opciones temas de la aplicacion y los informes
        lblTemaAplicacion = new JLabel("Tema de la aplicacion");
        lblTemaAplicacion.setBounds(60,100,250,18);
        lblInformes = new JLabel("Informes de la aplicacion");
        lblInformes.setBounds(60,240,250,18);
        panelOpciones.add(lblTemaAplicacion);
        panelOpciones.add(lblInformes);

        //Creamos los radio buttons
        rbTemaPredeterminado = new JRadioButton("Tema Predeterminado ");
        rbRap = new JRadioButton("Tema Rap      ");
        rbPop = new JRadioButton("Tema Pop");

        rbTemaPredeterminado.setActionCommand("predeterminado");
        rbRap.setActionCommand("rap");
        rbPop.setActionCommand("pop");

        //Creo un grupo de botones donde meto los tres radioButtons y pongo uno marcado
        ButtonGroup rbGroup = new ButtonGroup();
        rbGroup.add(rbTemaPredeterminado);
        rbGroup.add(rbRap);
        rbGroup.add(rbPop);
        rbTemaPredeterminado.setSelected(true);

        //Añado los comando de accion a los radioButtons
        rbTemaPredeterminado.setActionCommand("Predeterminado");
        rbRap.setActionCommand("Rap");
        rbPop.setActionCommand("Pop");

        //Añado los parametos de los botones y los añado al panel
        rbTemaPredeterminado.setBounds(60,140,200,18);
        rbRap.setBounds(60,170,200,18);
        rbPop.setBounds(60,200,200,18);
        panelOpciones.add(rbTemaPredeterminado);
        panelOpciones.add(rbRap);
        panelOpciones.add(rbPop);

        //Creacion de los botones de los informes de la aplicacion
        btnInformeReproduciones = new JButton("Cancion con mas reprocciones");
        btnInformesCantantes = new JButton("Discos de cada cantante");
        btnInformeReproduciones.setActionCommand("informeReproducciones");
        btnInformesCantantes.setActionCommand("informeCantantes");
        btnInformeReproduciones.setBounds(60,280,250,18);
        btnInformesCantantes.setBounds(60,320,250,18);
        panelOpciones.add(btnInformeReproduciones);
        panelOpciones.add(btnInformesCantantes);

        //Creo el boton para salir al login
        btnCambiatUsuario = new JButton("Cambiar Usuario");
        btnCambiatUsuario.setActionCommand("cambiarUsuario");
        btnCambiatUsuario.setBounds(60,800,160,18);
        panelOpciones.add(btnCambiatUsuario);

        //Opciones de usuarios
        tipoUsuarioGrupo = new ButtonGroup();
        listUsuariosApp = new JList();
        dlmUsuariosApp = new DefaultListModel<>();
        listUsuariosApp.setModel(dlmUsuariosApp);

        lblUsuarios = new JLabel("Usuarios:");
        lblUsuarios.setBounds(860,100,150,12);
        panelOpciones.add(lblUsuarios);

        panelListUsuarios = new JPanel();
        panelListUsuarios.setBounds(860,150,600,700);
        panelListUsuarios.add(listUsuariosApp);
        panelOpciones.add(panelListUsuarios);

        rbAdmin = new JRadioButton("Administrador");
        rbNormal = new JRadioButton("Normal");
        rbVisual = new JRadioButton("Visualizacion");

        lblTipoUsuario = new JLabel("Tipo de Usuario:");
        lblTipoUsuario.setBounds(1500,150,150,12);
        panelOpciones.add(lblTipoUsuario);

        rbAdmin.setBounds(1500,200,150,12);
        rbNormal.setBounds(1500,230,150,12);
        rbVisual.setBounds(1500,260,150,12);

        panelOpciones.add(rbAdmin);
        panelOpciones.add(rbNormal);
        panelOpciones.add(rbVisual);

        tipoUsuarioGrupo.add(rbAdmin);
        tipoUsuarioGrupo.add(rbNormal);
        tipoUsuarioGrupo.add(rbVisual);

        btnModificarUsuario = new JButton("Modificar Usuario");
        btnModificarUsuario.setActionCommand("modificarUsuario");
        btnModificarUsuario.setBounds(1500,700,150,18);
        panelOpciones.add(btnModificarUsuario);

        btnEliminarUsuario = new JButton("Eliminar Usuario");
        btnEliminarUsuario.setActionCommand("eliminarUsuario");
        btnEliminarUsuario.setBounds(1500,800,150,18);
        panelOpciones.add(btnEliminarUsuario);
    }

    /**
     * Metodo que cambia de color la vita al predeterminado
     */
    public void cambiarColorPredeterminado(){
        panelDiscograficas.setBackground(Color.WHITE);
        panelCantantes.setBackground(Color.WHITE);
        panelCanciones.setBackground(Color.WHITE);
        panelDiscos.setBackground(Color.WHITE);
        panelOpciones.setBackground(Color.WHITE);
    }
    /**
     * Metodo que cambia de color la vita al pop
     */
    public void cambiarColorPop(){
        panelDiscograficas.setBackground(Color.yellow);
        panelCantantes.setBackground(Color.yellow);
        panelCanciones.setBackground(Color.yellow);
        panelDiscos.setBackground(Color.yellow);
        panelOpciones.setBackground(Color.yellow);
    }
    /**
     * Metodo que cambia de color la vita al rap
     */
    public void cambiarColorRap(){
        panelDiscograficas.setBackground(Color.red);
        panelCantantes.setBackground(Color.red);
        panelCanciones.setBackground(Color.red);
        panelDiscos.setBackground(Color.red);
        panelOpciones.setBackground(Color.red);

    }

    /**
     * Metodo que cambia la ventan para un usuario admin
     */
    public void cambiarVistaAdmin(){
        btnEliminarUsuario.setVisible(true);
        lblUsuarios.setVisible(true);
        lblTipoUsuario.setVisible(true);
        panelListUsuarios.setVisible(true);
        btnModificarUsuario.setVisible(true);
        rbVisual.setVisible(true);
        rbAdmin.setVisible(true);
        rbNormal.setVisible(true);
        listUsuariosApp.setVisible(true);
        btnAnadirDiscografica.setVisible(true);
        btnModificarDiscografica.setVisible(true);
        btnEliminarDiscografica.setVisible(true);
        btnAnadirCantante.setVisible(true);
        btnModificarCantante.setVisible(true);
        btnEliminarCantante.setVisible(true);
        btnAnadirDisco.setVisible(true);
        btnModificarDisco.setVisible(true);
        btnEliminarDisco.setVisible(true);
        btnAnadirCancion.setVisible(true);
        btnModificarCancion.setVisible(true);
        btnEliminarCancion.setVisible(true);
    }

    /**
     * Metodo que cambia la ventan para un usuario normal
     */
     public void cambiarVistaNomal(){  btnEliminarUsuario.setVisible(true);
        btnEliminarUsuario.setVisible(false);
        lblUsuarios.setVisible(false);
        lblTipoUsuario.setVisible(false);
        panelListUsuarios.setVisible(false);
        btnModificarUsuario.setVisible(false);
        rbVisual.setVisible(false);
        rbAdmin.setVisible(false);
        rbNormal.setVisible(false);
        listUsuariosApp.setVisible(false);
        btnAnadirDiscografica.setVisible(true);
        btnModificarDiscografica.setVisible(true);
        btnEliminarDiscografica.setVisible(true);
        btnAnadirCantante.setVisible(true);
        btnModificarCantante.setVisible(true);
        btnEliminarCantante.setVisible(true);
        btnAnadirDisco.setVisible(true);
        btnModificarDisco.setVisible(true);
        btnEliminarDisco.setVisible(true);
        btnAnadirCancion.setVisible(true);
        btnModificarCancion.setVisible(true);
        btnEliminarCancion.setVisible(true);
     }

    /**
     * Metodo que cambia la ventan para un usuario visual
     */
     public  void cambiarVistaVisual(){
         btnEliminarUsuario.setVisible(false);
         lblUsuarios.setVisible(false);
         lblTipoUsuario.setVisible(false);
         panelListUsuarios.setVisible(false);
         btnModificarUsuario.setVisible(false);
         rbVisual.setVisible(false);
         rbAdmin.setVisible(false);
         rbNormal.setVisible(false);
         listUsuariosApp.setVisible(false);
         btnAnadirDiscografica.setVisible(false);
        btnModificarDiscografica.setVisible(false);
        btnEliminarDiscografica.setVisible(false);
        btnAnadirCantante.setVisible(false);
        btnModificarCantante.setVisible(false);
        btnEliminarCantante.setVisible(false);
        btnAnadirDisco.setVisible(false);
        btnModificarDisco.setVisible(false);
        btnEliminarDisco.setVisible(false);
        btnAnadirCancion.setVisible(false);
        btnModificarCancion.setVisible(false);
        btnEliminarCancion.setVisible(false);
     }


}