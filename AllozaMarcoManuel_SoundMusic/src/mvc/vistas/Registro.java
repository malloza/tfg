package mvc.vistas;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Clase Registro
 * Venatna de registro en los que introduciremos los datos del nuevo usuario y
 * lo guardara en la base de datos
 */
public class Registro extends JDialog {

    //Declaro los elementos que contendra la vista de registro
    private JPanel panelRegistro;
    JLabel lblNombre;
    JLabel lblApellido;
    JLabel lblFechaNacimiento;
    JLabel lblNumeroTelefono;
    JLabel lblNick;
    JLabel lblContrasena;
    JLabel lblContrasenaComparacion;
    JLabel lbltipoUsuario;
    public JTextField txtNombre;
    public JTextField txtApellido;
    public DatePicker dpFechaNacimiento;
    public JTextField txtTelefono;
    public JTextField txtNick;
    public JTextField txtTipoUsuario;
    public JPasswordField pwContrasena;
    public JPasswordField pwContrasenaComparacion;
    public JButton btnAceptar;
    public JButton btnCancelar;
    ButtonGroup grupoRegistro;
    public JRadioButton rbAdminRegistro;
    public JRadioButton rbNormalRegistro;
    public JRadioButton rbVisualRegistro;

    Font fuente;

    /**
     * Constructor Registrar
     */
    public Registro() {
        setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logo.jpg"));
        setTitle("Registro");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 500, 350);
        setSize(900, 450);
        panelRegistro = new JPanel();
        panelRegistro.setBackground(SystemColor.controlHighlight);
        panelRegistro.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(panelRegistro);
        panelRegistro.setLayout(null);

        panelRegistro();
        setVisible(true);
    }

    /**
     * Método que contiene el panel de registro
     */
    private void panelRegistro() {
        /*
         * Panel
         */
        JPanel panelRegistro = new JPanel();
        panelRegistro.setBounds(10, 10, 850, 350);
        this.panelRegistro.add(panelRegistro);
        panelRegistro.setLayout(null);

        /*
         * Icono
         */
        JLabel lblIcono = new JLabel("");
        lblIcono.setIcon(new ImageIcon("img/usuario.png"));
        lblIcono.setBounds(80, 15, 300, 150);
        panelRegistro.add(lblIcono);

        /*
         * Nombre
         */
        lblNombre = new JLabel("Nombre");
        lblNombre.setBounds(475, 70, 80, 14);
        panelRegistro.add(lblNombre);

        txtNombre = new JTextField();
        txtNombre.setText("");
        txtNombre.setColumns(10);
        txtNombre.setBounds(610, 70, 160, 20);
        panelRegistro.add(txtNombre);

        /*
         * Apellidos
         */
        lblApellido = new JLabel("Apellido");
        lblApellido.setBounds(475, 110, 80, 14);
        panelRegistro.add(lblApellido);

        txtApellido = new JTextField();
        txtApellido.setText("");
        txtApellido.setColumns(10);
        txtApellido.setBounds(610, 110, 160, 20);
        panelRegistro.add(txtApellido);

        //Fecha de nacimiento
        lblFechaNacimiento = new JLabel("Fecha de Nacimiento");
        lblFechaNacimiento.setBounds(475, 160, 120, 14);
        panelRegistro.add(lblFechaNacimiento);

        dpFechaNacimiento = new DatePicker();
        dpFechaNacimiento.setBounds(610, 159, 180, 30);
        panelRegistro.add(dpFechaNacimiento);

        //Telefono
        lblNumeroTelefono = new JLabel("Tel\u00E9fono");
        lblNumeroTelefono.setBounds(475, 210, 80, 14);
        panelRegistro.add(lblNumeroTelefono);

        txtTelefono = new JTextField();
        txtTelefono.setText("");
        txtApellido.setColumns(10);
        txtTelefono.setBounds(610, 210, 160, 20);
        panelRegistro.add(txtTelefono);

        //Tipo de usuario
        lbltipoUsuario = new JLabel("Tipo de Usuario:");
        lbltipoUsuario.setBounds(475,270,120,14);
        panelRegistro.add(lbltipoUsuario);

        grupoRegistro = new ButtonGroup();
        rbAdminRegistro = new JRadioButton("Administrador");
        rbNormalRegistro = new JRadioButton("Normal");
        rbVisualRegistro = new JRadioButton("Visualizar");
        grupoRegistro.add(rbAdminRegistro);
        grupoRegistro.add(rbVisualRegistro);
        grupoRegistro.add(rbNormalRegistro);
        rbAdminRegistro.setBounds(610,250,120,14);
        rbNormalRegistro.setBounds(610,270,120,14);
        rbVisualRegistro.setBounds(610,290,120,14);
        panelRegistro.add(rbAdminRegistro);
        panelRegistro.add(rbNormalRegistro);
        panelRegistro.add(rbVisualRegistro);

        //Nick
        lblNick = new JLabel("Usuario");
        lblNick.setBounds(40, 200, 110, 14);
        panelRegistro.add(lblNick);

        txtNick = new JTextField();
        txtNick.setText("");
        txtNick.setColumns(10);
        txtNick.setBounds(150, 200, 160, 20);
        panelRegistro.add(txtNick);

        //Contraseña
        lblContrasena  = new JLabel("Contrase\u00F1a");
        lblContrasena.setBounds(40, 230, 80, 14);
        panelRegistro.add(lblContrasena);

        pwContrasena = new JPasswordField();
        pwContrasena.setText("");
        pwContrasena.setColumns(10);
        pwContrasena.setBounds(150,230,160,20);
        panelRegistro.add(pwContrasena);

        //Contraseña de confirmación
        lblContrasenaComparacion = new JLabel("Repite Contrase\u00F1a");
        lblContrasenaComparacion.setBounds(40, 260, 120, 14);
        panelRegistro.add(lblContrasenaComparacion);

        pwContrasenaComparacion = new JPasswordField();
        pwContrasenaComparacion.setText("");
        pwContrasenaComparacion.setColumns(10);
        pwContrasenaComparacion.setBounds(150,260,160,20);
        panelRegistro.add(pwContrasenaComparacion);

        //Boton de aceptar
        btnAceptar = new JButton("Aceptar");
        btnAceptar.setActionCommand("aceptar");
        btnAceptar.setBounds(20, 377, 89, 23);
        this.panelRegistro.add(btnAceptar);

        //Boton de cancelar
        btnCancelar = new JButton("Cancelar");
        btnCancelar.setActionCommand("cancelar");
        btnCancelar.setBounds(715, 377, 89, 23);
        this.panelRegistro.add(btnCancelar);
        setLocationRelativeTo(null);
    }
}