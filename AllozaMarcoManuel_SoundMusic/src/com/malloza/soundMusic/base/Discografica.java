package com.malloza.soundMusic.base;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "discograficas", schema = "soundmusic")
public class Discografica {
    private int idDiscografica;
    private String nombre;
    private String direccion;
    private String codigoPostal;
    private String cif;
    private String numeroTelefono;
    private String correoElectronico;
    private String nombreDirector;
    private List<Cantante> cantantes;

    /**
     * Constructor clase discografica
     * @param nombre
     * @param direccion
     * @param codigoPostal
     * @param cif
     * @param numeroTelefono
     * @param correoElectronico
     */
    public Discografica(String nombre, String direccion, String codigoPostal, String cif, String numeroTelefono, String correoElectronico) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.codigoPostal = codigoPostal;
        this.cif = cif;
        this.numeroTelefono = numeroTelefono;
        this.correoElectronico = correoElectronico;
    }

    /**
     * Constructor vacio de la clase discografica
     */
    public  Discografica (){

    }

    @Override
    public String toString() {
        return "Nombre de la Discografica: "+nombre+"   Nombre del duerño: "+nombreDirector+"   Numero telefono: "+numeroTelefono+"   Código postal: "+codigoPostal;
    }

    @Id
    @Column(name = "idDiscografica")
    public int getIdDiscografica() {
        return idDiscografica;
    }

    public void setIdDiscografica(int idDiscografica) {
        this.idDiscografica = idDiscografica;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "codigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Basic
    @Column(name = "cif")
    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    @Basic
    @Column(name = "numeroTelefono")
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    @Basic
    @Column(name = "correoElectronico")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Basic
    @Column(name = "nombreDirector")
    public String getNombreDirector() {
        return nombreDirector;
    }

    public void setNombreDirector(String nombreDirector) {
        this.nombreDirector = nombreDirector;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Discografica that = (Discografica) o;
        return idDiscografica == that.idDiscografica &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(direccion, that.direccion) &&
                Objects.equals(codigoPostal, that.codigoPostal) &&
                Objects.equals(cif, that.cif) &&
                Objects.equals(numeroTelefono, that.numeroTelefono) &&
                Objects.equals(correoElectronico, that.correoElectronico) &&
                Objects.equals(nombreDirector, that.nombreDirector);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDiscografica, nombre, direccion, codigoPostal, cif, numeroTelefono, correoElectronico, nombreDirector);
    }

    @OneToMany(mappedBy = "discografica")
    public List<Cantante> getCantantes() {
        return cantantes;
    }

    public void setCantantes(List<Cantante> cantantes) {
        this.cantantes = cantantes;
    }
}
