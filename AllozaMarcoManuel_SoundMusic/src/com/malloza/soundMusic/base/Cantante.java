package com.malloza.soundMusic.base;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cantantes", schema = "soundmusic")
public class Cantante {
    private int idCantante;
    private String nombre;
    private LocalDate fechaNacimiento;
    private String nacionalidad;
    private String premios;
    private String genreo;
    private String sexo;
    private String formacion;
    private Discografica discografica;
    private List<Disco> discos;

    @Override
    public String toString() {
        return "Nombre: "+nombre+"   Fecha de nacimiento: "+fechaNacimiento+"   Nacionalidad: "+nacionalidad+"   Genero musical: "+genreo;
    }

    @Id
    @Column(name = "idCantante")
    public int getIdCantante() {
        return idCantante;
    }

    public void setIdCantante(int idCantante) {
        this.idCantante = idCantante;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fechaNacimiento")
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "nacionalidad")
    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @Basic
    @Column(name = "premios")
    public String getPremios() {
        return premios;
    }

    public void setPremios(String premios) {
        this.premios = premios;
    }

    @Basic
    @Column(name = "genreo")
    public String getGenreo() {
        return genreo;
    }

    public void setGenreo(String genreo) {
        this.genreo = genreo;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "formacion")
    public String getFormacion() {
        return formacion;
    }

    public void setFormacion(String formacion) {
        this.formacion = formacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cantante cantante = (Cantante) o;
        return idCantante == cantante.idCantante &&
                Objects.equals(nombre, cantante.nombre) &&
                Objects.equals(fechaNacimiento, cantante.fechaNacimiento) &&
                Objects.equals(nacionalidad, cantante.nacionalidad) &&
                Objects.equals(premios, cantante.premios) &&
                Objects.equals(genreo, cantante.genreo) &&
                Objects.equals(sexo, cantante.sexo) &&
                Objects.equals(formacion, cantante.formacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCantante, nombre, fechaNacimiento, nacionalidad, premios, genreo, sexo, formacion);
    }

    @ManyToOne
    @JoinColumn(name = "discografica_id", referencedColumnName = "idDiscografica")
    public Discografica getDiscografica() {
        return discografica;
    }

    public void setDiscografica(Discografica discografica) {
        this.discografica = discografica;
    }

    @OneToMany(mappedBy = "cantante")
    public List<Disco> getDiscos() {
        return discos;
    }

    public void setDiscos(List<Disco> discos) {
        this.discos = discos;
    }


}
