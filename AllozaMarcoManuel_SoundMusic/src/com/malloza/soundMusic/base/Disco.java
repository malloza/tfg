package com.malloza.soundMusic.base;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "discos", schema = "soundmusic")
public class Disco {
    private int idDisco;
    private String nombre;
    private String genero;
    private LocalDate fechaEstreno;
    private int numeroCanciones;
    private String duracion;
    private String featurings;
    private double precio;

    @Override
    public String toString() {
        return "Nombre: "+nombre+"   Fecha de estreno: "+fechaEstreno+"   Genero: "+ genero+"   Duracion disco: "+duracion;
    }

    private Cantante cantante;
    private List<Cancion> canciones;

    @Id
    @Column(name = "idDisco")
    public int getIdDisco() {
        return idDisco;
    }

    public void setIdDisco(int idDisco) {
        this.idDisco = idDisco;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "genero")
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Basic
    @Column(name = "fechaEstreno")
    public LocalDate getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(LocalDate fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    @Basic
    @Column(name = "numeroCanciones")
    public int getNumeroCanciones() {
        return numeroCanciones;
    }

    public void setNumeroCanciones(int numeroCanciones) {
        this.numeroCanciones = numeroCanciones;
    }

    @Basic
    @Column(name = "duracion")
    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    @Basic
    @Column(name = "featurings")
    public String getFeaturings() {
        return featurings;
    }

    public void setFeaturings(String featurings) {
        this.featurings = featurings;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disco disco = (Disco) o;
        return idDisco == disco.idDisco &&
                numeroCanciones == disco.numeroCanciones &&
                Double.compare(disco.precio, precio) == 0 &&
                Objects.equals(nombre, disco.nombre) &&
                Objects.equals(genero, disco.genero) &&
                Objects.equals(fechaEstreno, disco.fechaEstreno) &&
                Objects.equals(duracion, disco.duracion) &&
                Objects.equals(featurings, disco.featurings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDisco, nombre, genero, fechaEstreno, numeroCanciones, duracion, featurings, precio);
    }

    @ManyToOne
    @JoinColumn(name = "cantante_id", referencedColumnName = "idCantante")
    public Cantante getCantante() {
        return cantante;
    }

    public void setCantante(Cantante cantante) {
        this.cantante = cantante;
    }

    @OneToMany(mappedBy = "disco")
    public List<Cancion> getCanciones() {
        return canciones;
    }

    public void setCanciones(List<Cancion> canciones) {
        this.canciones = canciones;
    }
}
