package com.malloza.soundMusic.base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "canciones", schema = "soundmusic")
public class Cancion {
    private int idCancion;
    private String titulo;
    private String genero;
    private String duracion;
    private String featuring;
    private int reproducciones;
    private boolean explicita;
    private double precio;
    private Disco disco;

    @Override
    public String toString() {
        return "Titulo: "+titulo+"   Genero: "+genero+"   Reproducciones: "+reproducciones+"   Precio: "+precio;
    }

    @Id
    @Column(name = "idCancion")
    public int getIdCancion() {
        return idCancion;
    }

    public void setIdCancion(int idCancion) {
        this.idCancion = idCancion;
    }

    @Basic
    @Column(name = "titulo")
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Basic
    @Column(name = "genero")
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Basic
    @Column(name = "duracion")
    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    @Basic
    @Column(name = "featuring")
    public String getFeaturing() {
        return featuring;
    }

    public void setFeaturing(String featuring) {
        this.featuring = featuring;
    }

    @Basic
    @Column(name = "reproducciones")
    public int getReproducciones() {
        return reproducciones;
    }

    public void setReproducciones(int reproducciones) {
        this.reproducciones = reproducciones;
    }

    @Basic
    @Column(name = "explicita")
    public boolean getExplicita() {
        return explicita;
    }

    public void setExplicita(boolean explicita) {
        this.explicita = explicita;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cancion cancion = (Cancion) o;
        return idCancion == cancion.idCancion &&
                reproducciones == cancion.reproducciones &&
                explicita == cancion.explicita &&
                Double.compare(cancion.precio, precio) == 0 &&
                Objects.equals(titulo, cancion.titulo) &&
                Objects.equals(genero, cancion.genero) &&
                Objects.equals(duracion, cancion.duracion) &&
                Objects.equals(featuring, cancion.featuring);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCancion, titulo, genero, duracion, featuring, reproducciones, explicita, precio);
    }

    @ManyToOne
    @JoinColumn(name = "discos_id", referencedColumnName = "idDisco")
    public Disco getDisco() {
        return disco;
    }

    public void setDisco(Disco disco) {
        this.disco = disco;
    }

}
