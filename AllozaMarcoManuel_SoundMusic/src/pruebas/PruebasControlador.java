package pruebas;

import com.malloza.soundMusic.base.Discografica;
import mvc.Controlador;
import mvc.Modelo;
import mvc.vistas.Login;
import mvc.vistas.Registro;
import mvc.vistas.VistaMain;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PruebasControlador extends Controlador {
    private static Discografica discografica;
    private Login login;
    private Registro registro;
    private VistaMain vista;
    private Modelo modelo;
    private Controlador controlador;

    /**
     * Constructor de la clase
     */
    public PruebasControlador() {
        super();
        this.login = new Login();
        this.vista = new VistaMain();
        this.modelo = new Modelo();
        this.registro = new Registro();
        this.controlador = new Controlador(login,registro,vista,modelo);
    }

    @Before
    public void setUp() throws Exception {
        discografica =new Discografica("maximus","madre vedruna",
                "50010","15645465","655988445","gmail");
    }

    @Test
    public void iniciarAcceso(){
        login.txtUsuario.setText("manu");
        login.pwLogin.setText("1234");
        controlador.getLogin().btnAcceder.doClick();
    }

    @Test
    public void infoDiscografica(){
        vista.txtNombreDiscografica.setText(discografica.getNombre());
        vista.txtDireccion.setText(discografica.getDireccion());
        vista.txtCodigoPostal.setText(discografica.getCodigoPostal());
        vista.txtCif.setText(discografica.getCif());
        vista.txtTelefonoDiscografica.setText(discografica.getNumeroTelefono());
        vista.txtCorreoElectronico.setText(discografica.getCorreoElectronico());
        Assert.assertTrue("Prueba infromacion correcta",vista.txtDireccion.equals("madre vedruna"));
    }
}