package pruebas;

import com.malloza.soundMusic.base.Discografica;
import com.malloza.soundMusic.base.Usuario;
import mvc.Modelo;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class PruebasModelo extends Modelo {

    private static Discografica discografica;
    private static Usuario usuario;
    private static Usuario usuario1;

    @BeforeClass
    public static void setUpClass(){
        discografica = new Discografica("maximus","madre vedruna",
                "50010","15645465","655988445","gmail");
        usuario = new Usuario("manuel","alloza","948445447","administrador");
        usuario1 = new Usuario("juan","alloza","948445447","administrador");

    }

    @Test
    public void conectar() {
        this.conectar();
        //Assert.assertEquals(true, getSession().isOpen());
    }

    @Test
    public void pruebaAltaUsuario() {
       List lista = this.getUsuarios();
       int b = lista.size();
       this.altaUsuario(usuario);
       lista = this.getDiscograficas();
       Assert.assertTrue("Alta usuario",b+1 == lista.size());
       eliminarUsuario(usuario);
    }

    @Test
    public void pruebaAltaDiscografica() {
        List lista = this.getDiscograficas();
        int b = lista.size();
        this.altaDiscografica(discografica);
        lista = this.getDiscograficas();
        Assert.assertTrue("Alta disco", b + 1 == lista.size());
        eliminarDiscografica(discografica);

    }

    @Test
    public void pruebaModificarUsuario() {
        altaUsuario(usuario1);
        altaUsuario(usuario);
        usuario.setNombre("juan");
        modificarUsuario(usuario);
        Assert.assertEquals(usuario.getNombre(),usuario1.getNombre());
        eliminarUsuario(usuario);
        eliminarUsuario(usuario1);
    }

    @Test
    public void eliminarUsuario() {
        altaUsuario(usuario);
        int b = getUsuarios().size();
        this.eliminarUsuario(usuario);
        Assert.assertTrue("El usuario se ha eliminado correctamente",getUsuarios().size() == (b-1));
    }
}
