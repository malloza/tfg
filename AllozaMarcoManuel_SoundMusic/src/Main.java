import mvc.Controlador;
import mvc.Modelo;
import mvc.vistas.Login;
import mvc.vistas.Registro;
import mvc.vistas.VistaMain;

public  class Main{

    public static void main(final String[] args) throws Exception {
        VistaMain vista = new VistaMain();
        Login login = new Login();
        Registro registro = new Registro();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(login,registro,vista,modelo);
    }
}